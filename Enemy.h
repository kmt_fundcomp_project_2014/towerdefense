/* Katie Loughran
	Thom Behrens
	Matthew Staffelbach
 	
	Tower Defense Project
	
	Enemy Class Header File
*/



#ifndef ENEMY_H
#define ENEMY_H
# include <iostream>
# include "SDL/SDL.h"
# include <SDL/SDL_image.h>
#include <vector>
# include "griddy.h"

using namespace std;

class Enemy{
	public:
		Enemy( int=0 );										//Default constructor
	
		int getDamage();										//Get functions to read from private data members
		int getArmour();							
		int getReward();									
		int getType();
		SDL_Surface* getguy();
		SDL_Surface* getguy2();
		SDL_Rect getguyRect();

		void setSpeed( int );								//Set functions to write to private data members
		void setDamage( int );
		void setArmour( int );
		void setReward( int );
		void setType( int );
		void setguyRectx(int);
		void setguyRecty(int);

		void drawEnemy( int );							//Chooses the image, thus which enemy to draw, based on a given integer type
		void freeEnemy();									//Frees the SDL Surfaces guy and guy2
		void update();											//Updates all of the data members, used in constructor
		SDL_Surface* animate( SDL_Surface *&, vector<griddy>::iterator, int, int );	//Animates the enemy running across the path
	private:
		int damage;												// Damage: The number of Lives the enemy takes away if it reaches the end. 
		int armour;													// Armour: The strength of resistance against enemy attack
		int reward;													// Reward: The amount of money awarded for killing the particular enemy
		int type;														// Type: Which of 5 enemies it is, (0-4)
		SDL_Surface* guy;										// SDL_Surface which corresponds to one out of two motions for animation
		SDL_Surface* guy2;									// SDL_Surface which corresponds to the other one out of two motions for animation
		SDL_Rect guyRect;									//The SDL coordinates of the guy
};

#endif

