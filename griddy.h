//griddy.h


#ifndef GRIDDY_H
#define GRIDDY_H
 # include <iostream>
 #include <string>
using std::cout;
using std::endl;

using namespace std;


class griddy
{
	public:   // THESE ARE MY PUBLIC CLASSES
		griddy();// default constructor
		griddy(double, double, int, int, int, int);//constructor which adds some things
		int getxcoord();
		int getycoord();
		int getysize();
		int getxsize();
		int getimage();
		int getstat();
		void setxsize(int);
		void setysize(int);
		void setxcoord(int);
		void setycoord(int);
		void setimage(int);
		void setstat(int);
	private:
		int xcoord;
		int ycoord; 
		int image;
		int xsize;
		int ysize;
		int stat;
};

#endif
