#include "SDL/SDL.h"
#include <iostream>
#include "source_sdl.h"

using namespace std;
int source_sdl( int argc, char* args[] ) { 
	int x = 0;
	int y = 0;
   int running = 1;
   int treescroll = 3;
	int gallop = 0;
	int tower_down=0;
	int tx,ty;

 	//The images
	SDL_Surface* ground = NULL;
	SDL_Surface* guy = NULL;
	SDL_Surface* guy2 = NULL;
	SDL_Surface* guy3 = NULL;
	SDL_Surface* guy4 = NULL;
	SDL_Surface* tower = NULL;
	SDL_Surface* dome = NULL;
	SDL_Surface* tree1 = NULL;
	SDL_Surface* tree2 = NULL;
 	SDL_Surface* tree3 = NULL;
 	SDL_Surface* tree4 = NULL;
 	SDL_Surface* tree5 = NULL;
 	SDL_Surface* mountain = NULL;
 	SDL_Surface* midground = NULL;
 	SDL_Surface* screen = NULL;
 	SDL_Rect towerRect; // This is used to locate the placement of the tower later
	SDL_Rect domeRect;
 	SDL_Rect treelocationRect;
 	SDL_Rect mountainlocationRect;
 	SDL_Rect midgroundlocationRect;
 	SDL_Rect guylocationRect;
 	SDL_Rect guy2locationRect;
 	SDL_Rect groundlocationRect;
 	mountainlocationRect.x = 0;
 	mountainlocationRect.y = 0;
 	towerRect.x = x ;
 	towerRect.y = y ;
 	domeRect.x = x ;
 	domeRect.y = y ;
 	treelocationRect.x = 300;
 	treelocationRect.y = 300;
	guylocationRect.x = 300;
 	guylocationRect.y = 300;
 	guy2locationRect.x = 300;
 	guy2locationRect.y = 300;
 	groundlocationRect.x = 300;
 	groundlocationRect.y = 300;
 	midgroundlocationRect.x = 0;
 	midgroundlocationRect.y = 75;




	//Start SDL 
	SDL_Init( SDL_INIT_EVERYTHING );
  	if(  SDL_Init( SDL_INIT_EVERYTHING ) == -1) { 
  		running  = 0;
	}

	//Set up screen
 	screen = SDL_SetVideoMode( 875, 625, 32, SDL_SWSURFACE );
	if ( screen == NULL){
		running = 0;
  	}
	
	//Load image
	tower = SDL_LoadBMP( "fire_tower.bmp" );
	dome = SDL_LoadBMP( "dome.bmp" );
	guy = SDL_LoadBMP( "Knight.bmp" );
	guy2 = SDL_LoadBMP( "Knight2.bmp" );
	guy3 = SDL_LoadBMP( "guy_on_horse.bmp" );
	guy4 = SDL_LoadBMP( "guy_on_horse2.bmp" );
	ground = SDL_LoadBMP( "ground.bmp" );
	tree3 = SDL_LoadBMP( "treeUp.bmp");
	tree2 = SDL_LoadBMP( "treeLeftish.bmp");
	tree1 = SDL_LoadBMP( "treeLeft.bmp");
	tree4 = SDL_LoadBMP( "treeRightish.bmp");
	tree5 = SDL_LoadBMP( "treeRight.bmp");
	mountain = SDL_LoadBMP ("mountain.bmp" );
	midground = SDL_LoadBMP ("midground.bmp" );
 //if(tower == NULL || tree3 == NULL || tree2 == NULL || tree1 == NULL || tree4 == NULL || tree5 == NULL || mountain == NULL || midground){
 //   cout<<"failed to load an image"<<endl;
 // }

	SDL_Event occur;

	while (running == 1){

		while(SDL_PollEvent(&occur)){

			switch(occur.type){
				case SDL_QUIT:
	 				running = 0;
					break;
				case SDL_MOUSEBUTTONDOWN:
					if(occur.button.button == SDL_BUTTON_LEFT){
						x = occur.button.x;
						y = occur.button.y;
						towerRect.x = x ;
						towerRect.y = y ;
						tx=towerRect.x , ty=towerRect.y;
						tower_down=1;

						SDL_BlitSurface( tower, NULL, screen, &towerRect );  					//Apply image to screen  at given location
						cout<<"This is towerRect: "<<towerRect.x<<endl<<" This is x: "<<x<<endl; 			
					}
					break;
				case SDL_KEYDOWN:
					if(occur.key.keysym.sym == SDLK_q){
						running = 0;
					}
					break;
				default:
					break;
			}
		}

		if(tower_down == 1){
			//towerRect.x = tx ;
			//towerRect.y = ty ;
			SDL_BlitSurface( tower, NULL, screen, &towerRect );  					//Apply image to screen  at given location
		}	

 		//Update Screen
		SDL_Flip( screen );

		// Pause
 		SDL_Delay( 300 );
	
		//Mountain Animation
		for(int ti = 0; ti < 12; ti ++){
			SDL_BlitSurface( mountain, NULL, screen, &mountainlocationRect );
			mountainlocationRect.x = 75*ti;
		}

		//Midground Animation
		for(int re = 0; re < 25; re ++){
			SDL_BlitSurface( midground, NULL, screen, &midgroundlocationRect );
			midgroundlocationRect.x = 50*re;
		}

		//GUY ANIMATION
		if (gallop > 89) gallop=0;
		for( int yu = 0; yu<35; yu++){
			groundlocationRect.x=25*yu;
			groundlocationRect.y=5*25;
			SDL_BlitSurface( ground, NULL, screen, &groundlocationRect );
		}
		guylocationRect.x=10*gallop;
		guylocationRect.y=5*25;
		guy2locationRect.x=10*gallop-50;
		guy2locationRect.y=5*25;
		if( treescroll % 2 == 0 ){
			SDL_BlitSurface( guy, NULL, screen, &guylocationRect );
			SDL_BlitSurface( guy3, NULL, screen, &guy2locationRect );
		}
		else{
			SDL_BlitSurface( guy2, NULL, screen, &guylocationRect );
			SDL_BlitSurface( guy4, NULL, screen, &guy2locationRect );
		}
		gallop++;

		//GROUND ANIMATION
		treescroll++;
		for(int u = 6; u < 25; u++){
			for(int yu = 0; yu < 35; yu++){	

				groundlocationRect.x = 25*yu;
			 	groundlocationRect.y = 25*u;
				SDL_BlitSurface( ground, NULL, screen, &groundlocationRect );

			}
		}

		domeRect.x=0;
		domeRect.y=4*25;
		SDL_BlitSurface( dome, NULL, screen, &domeRect );
	
	
	}
	//Quit SDL
	//Free the loaded image
	SDL_FreeSurface( tower ); SDL_FreeSurface( guy ); SDL_FreeSurface( guy2 ); SDL_FreeSurface( ground );
	SDL_FreeSurface( tree1 );SDL_FreeSurface( tree2 );SDL_FreeSurface( tree3 );SDL_FreeSurface( tree4 );SDL_FreeSurface( tree5 );
	SDL_FreeSurface( mountain ); SDL_FreeSurface( dome );
	SDL_Quit();
	return 0;
}




