//The music that will be played
 Mix_Music *music = NULL;
 //The sound effects that will be used
 Mix_Chunk *scratch = NULL;
 Mix_Chunk *high = NULL;
 Mix_Chunk *med = NULL;
 Mix_Chunk *low = NULL;

//Initialize SDL_ttf
 if( TTF_Init() == -1 ) { return false; } 
//Initialize SDL_mixer
 if( Mix_OpenAudio( 22050, MIX_DEFAULT_FORMAT, 2, 4096 ) == -1 ) { return false; }
