/* Katie Loughran
	Thom Behrens
	Matthew Staffelbach
 	
	Tower Defense Project
	
	Area of Effect (AOE) Tower Class Implemntation
*/

# include <iostream>
# include "SDL/SDL.h"
# include <SDL/SDL_image.h>
#include <vector>
# include "griddy.h"
# include "Tower.h"
# include "AOETower.h"
# include <cmath>

using namespace std;

//\\// Area of Effect Tower Class Constructor

AOETower::AOETower( int lev, int x, int y ): Tower( lev ){
	setType(1);
	drawTower( getType());
	blast_radius=1;
	blast_multiplier = 2.5;
	settowerRectx(x);
	settowerRecty(y);
	Upgrade();
	bomb = IMG_Load("Bomb.png");
	explosion = IMG_Load("HeHeBigExplosion.png");
	
}

void AOETower::Upgrade(){				//extension of basic constructor
		setRange(2);
		setDamage(3);
		blast_radius=2*blast_multiplier;
		setTowerCost(2);
}
//\\//
int AOETower::getBlastRadius(){
	return blast_radius;
}
//\\//
void AOETower::Attack(vector <Enemy>& enemy_vec, SDL_Surface * screen, int treescroll) {
	int fire = 0;
	int itr  = 0;
	for(itr = 0; itr <= enemy_vec.size(); itr++) {
		if(getRange() >= sqrt(pow(abs(enemy_vec[itr].getguyRect().x-gettowerRect().x),2)+pow(abs(enemy_vec[itr].getguyRect().y-gettowerRect().y),2)) && enemy_vec[itr].getType() != 5) { // same as AOETower and SHOTTower
			fire = 1;
			break;
		}
	}
	int x = enemy_vec[itr].getguyRect().x;
	int y = enemy_vec[itr].getguyRect().y; // keeps track of enemy to shoot at,
	if (fire) {
		sendShot(enemy_vec[itr].getguyRect(), screen, treescroll);
		for(itr = 0; itr <= enemy_vec.size(); itr++) { // sees if any other enemies are within the range of the originally targeted enemy.
			if(sqrt(pow(abs(enemy_vec[itr].getguyRect().x-x),2)+pow(abs(enemy_vec[itr].getguyRect().y-y),2)) <= 50  && enemy_vec[itr].getType() != 5 ) {
				enemy_vec[itr].setArmour(enemy_vec[itr].getArmour() - (getDamage()*2));
			}
		}
	}
}
//\\//
void AOETower::sendShot( SDL_Rect guyRect, SDL_Surface * screen, int treescroll){
	int xdist = abs(guyRect.x - gettowerRect().x);
	int ydist = abs(guyRect.y - gettowerRect().y);
	int xsign = sign(gettowerRect().x, guyRect.x);
	int ysign = sign(gettowerRect().y, guyRect.y);
	int shotdist = 0;
	int enemydist = sqrt(pow(abs(guyRect.x-gettowerRect().x),2)+pow(abs(guyRect.y-gettowerRect().y),2));
	switch (treescroll%3) {
		case 0:
			shotRect.x = gettowerRect().x + xsign * xdist / 3;
			shotRect.y = gettowerRect().y + ysign * ydist / 3;
			SDL_BlitSurface( bomb, NULL, screen, &shotRect );
			SDL_Flip( screen );
			SDL_Delay(2);
			break;
		case 1:
			shotRect.x = gettowerRect().x + xsign * 2 * xdist / 3;
			shotRect.y = gettowerRect().y + ysign * 2 * ydist / 3; // sends bomb over time based on treescroll variable
			SDL_BlitSurface( bomb, NULL, screen, &shotRect );
			SDL_Flip( screen );
			SDL_Delay(2);
			break;
		case 2:
			SDL_BlitSurface( explosion, NULL, screen, &guyRect );
			SDL_Flip( screen );
			SDL_Delay(2);
			break;
		default:
			break;
	}
}
//\\//
int AOETower::sign(int from, int to) {
 if(from > to) return -1;
 else return 1;
}
