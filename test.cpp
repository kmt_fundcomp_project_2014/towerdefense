//Katie Loughran
//Just testing Tower Hierarchy
//4/19/2014

# include <iostream>
# include "Tower.h"
# include "AOETower.h"
# include "PULSTower.h"
# include "SHOTTower.h"
# include "Enemy.h"

using namespace std;

int main(){

	cout << "hello?"<<endl;
	AOETower aoe_tower(0);
	PULSTower p_tower(1);
	SHOTTower s_tower(3);
	Enemy e1(0);
	Enemy e2(1);
	Enemy e3(2);
	Enemy e4(3);
	Enemy e5(4);


	cout << "Area of effect blast radius = " << aoe_tower.getBlastRadius() << endl;
	cout << "Pulse slow enemies = "<< p_tower.getSlowEnemies() << endl;
	cout << "Range of shooter = " << s_tower.getRange() << endl;
	cout<<endl<<endl;

	cout << "E1 armour = "<< e1.getArmour() << endl;
	cout << "E2 armour = "<< e2.getArmour() << endl;
	cout << "E3 armour = "<< e3.getArmour() << endl;
	cout << "E4 armour = "<< e4.getArmour() << endl;
	cout << "E5 armour = "<< e5.getArmour() << endl;

	cout << "AOE xpos= " << aoe_tower.getX() << endl;
	aoe_tower.setX( 500 );
	cout << "Changed AOE xpos to 500. "<< endl;
	cout << "AOE xpos = " << aoe_tower.getX() << endl;


	cout << "AOE ypos= " << aoe_tower.getY() << endl;
	aoe_tower.setY( 500 );
	cout << "Changed AOE ypos to 500. "<< endl;
	cout << "AOE ypos = " << aoe_tower.getY() << endl;

}
