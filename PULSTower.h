/* Katie Loughran
	Thom Behrens
	Matthew Staffelbach
 	
	Tower Defense Project
	
	Pulse Tower Class Header File
*/



#ifndef PULSTOWER_H
#define PULSTOWER_H

# include <iostream>
# include "SDL/SDL.h"
# include <SDL/SDL_image.h>
#include <vector>
# include "griddy.h"

using namespace std;

class PULSTower: public Tower{
	public:
		PULSTower(int = 0, int = 0, int = 0);
		
		virtual void Upgrade();
		int getSlowEnemies();		
		virtual void Attack(vector <Enemy>&, SDL_Surface *, int); // searches for enemy and calls sendShot function
		void sendShot( SDL_Rect, SDL_Surface *, int); // prints a pulse being fired on the screen

	private:
		SDL_Surface * smallPulse;
		SDL_Surface * medPulse; // images for different sizes of rays are only printed one at a time, so they only need one image.
		SDL_Surface * bigPulse;
		SDL_Rect PulseRect;

};

#endif
