#ifndef PATHVECFUNCTIONS_H
#define PATHVECFUNCTIONS_H

#include <vector>				

void makepathtwo(int map, vector<vector<griddy> > &vec2D, vector<griddy> &path_vec);		//This function initializes a path based on a map text file
																																																	//and creates a path vector for enemies to follow
void placetrees(vector<vector<griddy> > &vec2D, vector<griddy> &path_vec);									//This function decides where to place trees and changes the 2D
																																																	//vector that keeps track of the board

#endif
