/* Katie Loughran
	Thom Behrens
	Matthew Staffelbach
 	
	Tower Defense Project
	
	Shooter Tower Class Header File
*/
#ifndef SHOTTOWER_H
#define SHOTTOWER_H

# include <iostream>
# include "SDL/SDL.h"
# include <SDL/SDL_image.h>
#include <vector>
# include "griddy.h"
# include "Enemy.h"

using namespace std;

class SHOTTower: public Tower{
	public:
		SHOTTower(int = 0, int = 0, int = 0);
		
		virtual void Upgrade();
		virtual void Attack(vector <Enemy>&, SDL_Surface *, int); // searches for enemy and calls sendShot function
		void sendShot( SDL_Rect, SDL_Surface *, int); // prints a shot being fired on the screen
		int sign(int, int); // find direction in which shot should fire
	private:
		SDL_Surface * shot; // image and location for bullets that make up the "laser beam"
		SDL_Rect shotRect;
};

#endif
