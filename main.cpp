#include "SDL/SDL.h"
#include <SDL/SDL_image.h>
#include <string>
#include <iostream>
#include <vector>
#include <string>
#include "griddy.h"
#include <sstream>
#include <stdio.h>      /* printf, NULL */
#include <stdlib.h>     /* srand, rand */
#include <time.h>       /* time */
#include "src.h"
#include "Tower.h"
#include "AOETower.h"
#include "PULSTower.h"
#include "SHOTTower.h"
#include "Enemy.h"
#include "tdfunctions.h"
#include "pathVecFunc.h"
#include "SDL/SDL_mixer.h"


using namespace std;
int main(int argc, char* args[] ){ 
	srand (time(NULL));
	int wizardtowercost = 50;
	int firetowercost = 50;
	int shottowercost = 50;
	int numberoffiretower = 0;
	int numberofwizardtower = 0;
	int numberofshottower = 0;
	int runningMenu = 1;
	int running = 0;
	int runningWhole = 1;
	int runningBestiary = 0;
	int map = 0;
	int runningInstructions = 0;
	int runningCredits = 0;
	int treescroll = 3;
	int x=0; int y=0;
	int place_tow;
	griddy blocks;
	vector<vector<griddy> > vec2D;
	vector<griddy> path_vec;
	vector<Tower*> tower_vec;
	vector<Enemy> enemy_vec;
	string line;
	griddy value;
	int theimage = 3;

	int LEVEL=1, money_count=100, Lives=3, Enemies_Left=0;

	//______________Create_2D_Vector____________________

	for(int hk = 0; hk < 35; ++hk){
		vector<griddy> newLine;
		for(int jk = 0; jk < 25; ++jk){
			newLine.push_back(value);
		}
		vec2D.push_back(newLine);
	}


	//Initialize SDL_mixer
	if( Mix_OpenAudio( 22050, MIX_DEFAULT_FORMAT, 2, 4096 ) == -1 ) { return false; } 

	Mix_Music *music3 = NULL;

	//The sound effects that will be used 
	Mix_Chunk *shoot = NULL; 
	Mix_Chunk *missile = NULL; 
	Mix_Chunk *ray = NULL; 
	Mix_Chunk *death = NULL;

	shoot = Mix_LoadWAV( "shoot.wav" );
	missile = Mix_LoadWAV( "click.wav" );
	ray = Mix_LoadWAV( "ray.wav" );
	death = Mix_LoadWAV( "place.wav" );
	//If there was a problem loading the sound effects 
	if( ( shoot == NULL )){cout<<"This did not load shoot.wav"<<endl; }
	if( ( missile == NULL ) || ( ray == NULL ) || ( death == NULL ) ) {cout<<"This did not load all the sound effects"<<endl; }

	//Load the music

	music3 = Mix_LoadMUS( "bestmusic.wav" );

	//Load up The images
	SDL_Surface* menu = NULL;
	SDL_Surface* dome = NULL;
	SDL_Surface* credits = NULL;
	SDL_Surface* win = NULL;
	SDL_Surface* lose = NULL;
	SDL_Surface* midground = NULL;
	SDL_Surface* ground = NULL;
	SDL_Surface* picture = NULL; 
	SDL_Surface* pathRight = NULL;
	SDL_Surface* pathDownRight = NULL;
	SDL_Surface* pathLeftDown = NULL;
	SDL_Surface* pathLeftUp = NULL;
	SDL_Surface* pathUpDown = NULL;
	SDL_Surface* pathUpRight = NULL;
	SDL_Surface* screen = NULL;
	SDL_Surface* instructions = NULL;
	SDL_Surface* occupiedGround = NULL;
	SDL_Surface* bestiary = NULL;
	SDL_Surface* treeUp = NULL;
	SDL_Surface* treeLeft = NULL;
	SDL_Surface* treeLeftish = NULL;
	SDL_Surface* treeRight = NULL;
	SDL_Surface* treeRightish = NULL;
	bestiary = IMG_Load ( "bestiary.png" );
	credits = IMG_Load( "credits.png" );
	instructions = IMG_Load( "instructions.png" );
	midground = SDL_LoadBMP ("midground.bmp" ); //2
	dome = SDL_LoadBMP ("dome.png" ); 
	ground = SDL_LoadBMP ("ground.bmp");//4
	pathRight = SDL_LoadBMP ("pathRight.bmp"); //5
	pathLeftDown = SDL_LoadBMP("pathLeftDown.bmp");//6
	pathLeftUp = SDL_LoadBMP("pathLeftUp.bmp");//7
	pathDownRight = SDL_LoadBMP("pathDownRight.bmp");//8
	pathUpRight = SDL_LoadBMP("pathUpRight.bmp");//9
	pathUpDown = SDL_LoadBMP("pathUpDown.bmp");//10
	menu = IMG_Load( "background.png" ); // 16
	win = IMG_Load( "win.png" );
	lose = IMG_Load( "lose.png" );
	occupiedGround = SDL_LoadBMP("occupiedGround.bmp"); // 17
	treeUp = SDL_LoadBMP( "treeUp.bmp"); //3
	treeLeftish = SDL_LoadBMP( "treeLeftish.bmp"); //11
	treeLeft = SDL_LoadBMP( "treeLeft.bmp");//12
	treeRightish = SDL_LoadBMP( "treeRightish.bmp");//13
	treeRight = SDL_LoadBMP( "treeRight.bmp");//14

	if(instructions == NULL || midground == NULL || ground == NULL || pathRight == NULL || pathLeftDown == NULL || pathLeftUp == NULL ||
	pathDownRight== NULL || pathUpRight == NULL || pathUpDown == NULL || 
	occupiedGround == NULL || menu == NULL || win == NULL || lose == NULL ) {cout<<"failed to load an image"<<endl;}


	SDL_Rect treelocationRect;
	SDL_Rect menuRect;
	menuRect.x = 0;
	menuRect.y = 0;

	//Start SDL 
	running=start_SDL();
	//Set up screen
	screen = SDL_SetVideoMode( 875, 625, 32, SDL_SWSURFACE );
	if ( screen == NULL){
		running = 0;
	}
	//Load image

	SDL_Event occur;
	SDL_Event res;




	//______________Create_Enemy_Vectors________________

	int wave=1;
	create_enemies(wave, enemy_vec);

	//_______________________________________________________

	makegrounddefault(vec2D);
	makemountains(vec2D, screen);
	makemidground(vec2D);


	while (!path_vec.empty() ){
		path_vec.pop_back();
	}

	// Making a Path
	makepathtwo(1, vec2D, path_vec);
	makeNULLS(vec2D);
	placetrees(vec2D, path_vec);

	//Set Enemies Left
	vector<Enemy>::iterator em;
	em = enemy_vec.begin();
	for (em = enemy_vec.begin() ; em != enemy_vec.end() ; em ++ ){
		if( em->getType() != 5) Enemies_Left ++;
	}	


	//These are iterators to run through path, enemy, and tower vectors
	vector<griddy>::iterator i;
	i = path_vec.begin();
	vector<Tower*>::iterator j;
	j = tower_vec.begin();


	vector<Enemy>::iterator en;
	en = enemy_vec.begin();
	int control=0;
	//end iterators



	//____________________________MENU CODE_________________________________________
	while (runningWhole == 1 ) {
		while( runningMenu == 1 ) {
			SDL_BlitSurface( menu, NULL, screen, &menuRect );
			while( SDL_PollEvent( &occur ) ){
				switch( occur.type ){
					case SDL_QUIT:
						runningMenu = 0;
						running = 0;
						runningWhole = 0;
						runningBestiary = 0;
						runningInstructions = 0;
						runningCredits = 0;
					case SDL_MOUSEBUTTONDOWN:
						switch( occur.button.button ){
							case SDL_BUTTON_LEFT:
								x = occur.button.x;
								y = occur.button.y;
								if( x < 500 && x > 400 && y > 210 && y < 250) // this is the 
													      //location of the start text
								{
									runningMenu = 0;
									running = 1;
									runningWhole = 1;
									runningBestiary = 0;
									runningInstructions = 0;
									runningCredits = 0;
								}
								if( x < 515 && x > 380 && y > 380 && y < 415) // this is the location of the start text
								{
									runningMenu = 0;
									running = 0;
									runningWhole = 1;
									runningBestiary = 0;
									runningInstructions = 0;
									runningCredits = 1;
								}
								if( x < 570 && x > 345 && y > 290 && y < 325) // This the location of the instrucitons text
								{
									runningMenu = 0;
									running = 0;
									runningWhole = 1;
									runningBestiary = 0;
									runningInstructions = 1;
									runningCredits = 0;
								}	
								if( x < 575 && x > 335 && y > 445 && y < 480) // This the location of the bestiary text
								{
									runningMenu = 0;
									running = 0;
									runningWhole = 1;
									runningBestiary = 1;
									runningInstructions = 0;
									runningCredits = 0;
								}		
								x = (x - x%25)/25; // this just makes the x and y equal places in the vec2D
								y = (y - y%25)/25;
								/* Look for a keypress */
						}
					case SDL_KEYDOWN:
						if(occur.key.keysym.sym == SDLK_q){
							runningMenu = 0;
							running = 0;
							runningWhole = 0;
							runningBestiary = 0;
							runningInstructions = 0;
							runningCredits = 0;
						}
						//If 9 was pressed else
						if( occur.key.keysym.sym == SDLK_9 ) { 
							//If there is no music playing
							if( Mix_PlayingMusic() == 0 ) {
								//Play the music
								if( Mix_PlayMusic( music3, -1 ) == -1 ) {
									return 1;
								}
							} else{
								//If music is being played
								//If the music is paused
								if( Mix_PausedMusic() == 1 ) {
								//Resume the music
								Mix_ResumeMusic();
								} else { 
									//If the music is playing 
									//Pause the music 
									Mix_PauseMusic();
								}
							}
						}
						//If 0 was pressed
						if( occur.key.keysym.sym == SDLK_0 ) {
							//Stop the music
							Mix_HaltMusic();
						}
						/* Check the SDLKey values and move change the coords */
						switch( occur.key.keysym.sym ){
							case SDLK_SPACE:
								runningMenu = 0;
								running = 0;
								runningWhole = 0;
								runningBestiary = 0;
								runningInstructions = 0;
								runningCredits = 0;
								break;
							default:
								break;
						}
				}
			}

			SDL_Flip( screen );
			// Pause
			SDL_Delay( 300 );
			if (runningWhole == 0){
				runningMenu = 0;
				running = 0;
				runningBestiary = 0;
				runningInstructions = 0;
				runningCredits = 0;
				SDL_FreeSurface( bestiary ); 
				SDL_FreeSurface( credits ); 
				SDL_FreeSurface( ground ); 
				SDL_FreeSurface( midground ); 
				SDL_FreeSurface( picture );
				SDL_FreeSurface( menu );
				SDL_FreeSurface( lose ); 
				SDL_FreeSurface( win );
				SDL_FreeSurface( occupiedGround ); 
				SDL_FreeSurface( pathRight ); 
				SDL_FreeSurface( pathDownRight ); 
				SDL_FreeSurface( pathLeftDown ); 
				SDL_FreeSurface( pathLeftUp ); 
				SDL_FreeSurface( pathUpDown ); 
				SDL_FreeSurface( pathUpRight ); 
				SDL_FreeSurface( screen ); 
				SDL_FreeSurface( instructions ); //Free the sound effects
				Mix_FreeChunk( shoot );
				Mix_FreeChunk( missile );
				Mix_FreeChunk( death );
				Mix_FreeChunk( ray );
				//Free the music
				Mix_FreeMusic( music3 );
			}
		}
		while( runningInstructions == 1 ){
			SDL_Rect instructionsRect;
			instructionsRect.x = 0;
			instructionsRect.y = 0;
			SDL_BlitSurface( instructions, NULL, screen, &instructionsRect );
			while( SDL_PollEvent( &occur ) ){
				switch( occur.type ){
					case SDL_QUIT:
						runningMenu = 0;
						running = 0;
						runningWhole = 0;
						runningBestiary = 0;
						runningInstructions = 0;
						runningCredits = 0;
					case SDL_MOUSEBUTTONDOWN:
						switch( occur.button.button ){
							case SDL_BUTTON_LEFT:
								x = occur.button.x;
								y = occur.button.y;
								/* Look for a keypress */
						}
					case SDL_KEYDOWN:
						if(occur.key.keysym.sym == SDLK_q){
							runningMenu = 0;
							running = 0;
							runningWhole = 0;
							runningBestiary = 0;
							runningInstructions = 0;
							runningCredits = 0;
						}
					//If 9 was pressed else
					if( occur.key.keysym.sym == SDLK_9 ) { 
					//If there is no music playing
						if( Mix_PlayingMusic() == 0 ) {
							//Play the music
							if( Mix_PlayMusic( music3, -1 ) == -1 ) {
							return 1;
							}
						} else {
							//If music is being played
							//If the music is paused
							if( Mix_PausedMusic() == 1 ) {
								//Resume the music
								Mix_ResumeMusic();
							} 	else { 
								//If the music is playing 
								//Pause the music 
								Mix_PauseMusic();
							}
						}
					} else if( occur.key.keysym.sym == SDLK_0 ) { //If 0 was pressed
						//Stop the music
						Mix_HaltMusic();
					}
					/* Check the SDLKey values and move change the coords */
					switch( occur.key.keysym.sym ){
						case SDLK_SPACE:
							runningMenu = 1;
							running = 0;
							runningWhole = 1;
							runningBestiary = 0;
							runningInstructions = 0;
							runningCredits = 0;
							break;
						default:
							break;
					}
				}
			}
			SDL_Flip( screen );
			// Pause
			SDL_Delay( 300 );
			if (runningWhole == 0){ 
				runningMenu = 0;
				running = 0;
				runningBestiary = 0;
				runningInstructions = 0;
				runningCredits = 0;
				SDL_FreeSurface( bestiary ); 
				SDL_FreeSurface( credits ); 
				SDL_FreeSurface( ground ); 
				SDL_FreeSurface( midground ); 
				SDL_FreeSurface( picture );
				SDL_FreeSurface( menu );
				SDL_FreeSurface( lose ); 
				SDL_FreeSurface( win );
				SDL_FreeSurface( occupiedGround ); 
				SDL_FreeSurface( pathRight ); 
				SDL_FreeSurface( pathDownRight ); 
				SDL_FreeSurface( pathLeftDown ); 
				SDL_FreeSurface( pathLeftUp ); 
				SDL_FreeSurface( pathUpDown ); 
				SDL_FreeSurface( pathUpRight ); 
				SDL_FreeSurface( screen ); 
				SDL_FreeSurface( instructions );//Free the sound effects
				Mix_FreeChunk( shoot );
				Mix_FreeChunk( missile );
				Mix_FreeChunk( death );
				Mix_FreeChunk( ray );
				//Free the music
				Mix_FreeMusic( music3 );

				Mix_CloseAudio();	
				//	SDL_Quit();
			}
		}


		while( runningBestiary == 1 ){
			SDL_Rect instructionsRect;
			instructionsRect.x = 0;
			instructionsRect.y = 0;
			SDL_BlitSurface( bestiary, NULL, screen, &instructionsRect );
			while( SDL_PollEvent( &occur ) ){
				switch( occur.type ){
					case SDL_QUIT:
						runningMenu = 0;
						running = 0;
						runningWhole = 0;
						runningBestiary = 0;
						runningInstructions = 0;
						runningCredits = 0;
					case SDL_MOUSEBUTTONDOWN:
						switch( occur.button.button ){
							case SDL_BUTTON_LEFT:
								x = occur.button.x;
								y = occur.button.y;
								/* Look for a keypress */
						}
					case SDL_KEYDOWN:
						if(occur.key.keysym.sym == SDLK_q){
							runningMenu = 0;
							running = 0;
							runningWhole = 0;
							runningBestiary = 0;
							runningInstructions = 0;
							runningCredits = 0;
						}
						//If 9 was pressed else
						if( occur.key.keysym.sym == SDLK_9 ) { 
							//If there is no music playing
							if( Mix_PlayingMusic() == 0 ) {
								//Play the music
								if( Mix_PlayMusic( music3, -1 ) == -1 ) {
									return 1;
								}
							} else { 
								//If music is being played
								//If the music is paused
								if( Mix_PausedMusic() == 1 ) {
									//Resume the music
									Mix_ResumeMusic();
								} else { 
									//If the music is playing 
									//Pause the music 
									Mix_PauseMusic();
								}
							}
						} else if( occur.key.keysym.sym == SDLK_0 ) { //If 0 was pressed
							//Stop the music
							Mix_HaltMusic();
						}
						/* Check the SDLKey values and move change the coords */
						switch( occur.key.keysym.sym ){
							case SDLK_SPACE:
								runningMenu = 1;
								running = 0;
								runningWhole = 1;
								runningBestiary = 0;
								runningInstructions = 0;
								runningCredits = 0;
								break;
							default:
								break;
						}
				}
			}
			SDL_Flip( screen );
			// Pause
			SDL_Delay( 300 );
			if (runningWhole == 0){
				runningMenu = 0;
				running = 0;
				runningBestiary = 0;
				runningInstructions = 0;
				runningCredits = 0; 
				SDL_FreeSurface( bestiary ); 
				SDL_FreeSurface( credits ); 
				SDL_FreeSurface( ground ); 
				SDL_FreeSurface( midground ); 
				SDL_FreeSurface( picture );
				SDL_FreeSurface( menu );
				SDL_FreeSurface( lose ); 
				SDL_FreeSurface( win );
				SDL_FreeSurface( occupiedGround ); 
				SDL_FreeSurface( pathRight ); 
				SDL_FreeSurface( pathDownRight ); 
				SDL_FreeSurface( pathLeftDown ); 
				SDL_FreeSurface( pathLeftUp ); 
				SDL_FreeSurface( pathUpDown ); 
				SDL_FreeSurface( pathUpRight ); 
				SDL_FreeSurface( screen ); 
				SDL_FreeSurface( instructions );//Free the sound effects
				Mix_FreeChunk( shoot );
				Mix_FreeChunk( missile );
				Mix_FreeChunk( death );
				Mix_FreeChunk( ray );
				//Free the music
				Mix_FreeMusic( music3 );
			}
		}

		while( runningCredits == 1 ){
			SDL_Rect instructionsRect;
			instructionsRect.x = 0;
			instructionsRect.y = 0;
			SDL_BlitSurface( credits, NULL, screen, &instructionsRect );
			while( SDL_PollEvent( &occur ) ){
				switch( occur.type ){
					case SDL_QUIT:
						runningMenu = 0;
						running = 0;
						runningWhole = 0;
						runningBestiary = 0;
						runningInstructions = 0;
						runningCredits = 0;
					case SDL_MOUSEBUTTONDOWN:
						switch( occur.button.button ){
							case SDL_BUTTON_LEFT:
								x = occur.button.x;
								y = occur.button.y;
								/* Look for a keypress */
						}
					case SDL_KEYDOWN:
						if(occur.key.keysym.sym == SDLK_q){
						runningMenu = 0;
						running = 0;
						runningWhole = 0;
						runningBestiary = 0;
						runningInstructions = 0;
						runningCredits = 0;
						}
					//If 9 was pressed else
					if( occur.key.keysym.sym == SDLK_9 ) { 
						//If there is no music playing
						if( Mix_PlayingMusic() == 0 ) {
							//Play the music
							if( Mix_PlayMusic( music3, -1 ) == -1 ) {
								return 1;
							}
						} 	else {
							//If music is being played
							//If the music is paused
							if( Mix_PausedMusic() == 1 ) {
								//Resume the music
								Mix_ResumeMusic();
							} else {
								//If the music is playing 
								 
								//Pause the music 
								Mix_PauseMusic();
							}
						}
					}
					//If 0 was pressed
					if( occur.key.keysym.sym == SDLK_0 ) {
						//Stop the music
						Mix_HaltMusic();
					}
					/* Check the SDLKey values and move change the coords */
					switch( occur.key.keysym.sym ){
						case SDLK_SPACE:
							runningMenu = 1;
							running = 0;
							runningWhole = 1;
							runningBestiary = 0;
							runningInstructions = 0;
							runningCredits = 0;
							break;
						default:
							break;
					}
				}
			}
			SDL_Flip( screen );
			// Pause
			SDL_Delay( 300 );
			if (runningWhole == 0){
				runningMenu = 0;
				running = 0;
				runningBestiary = 0;
				runningInstructions = 0;
				runningCredits = 0;
				SDL_FreeSurface( bestiary ); 
				SDL_FreeSurface( credits ); 
				SDL_FreeSurface( ground ); 
				SDL_FreeSurface( midground ); 
				SDL_FreeSurface( picture );
				SDL_FreeSurface( menu );
				SDL_FreeSurface( lose ); 
				SDL_FreeSurface( win );
				SDL_FreeSurface( occupiedGround ); 
				SDL_FreeSurface( pathRight ); 
				SDL_FreeSurface( pathDownRight ); 
				SDL_FreeSurface( pathLeftDown ); 
				SDL_FreeSurface( pathLeftUp ); 
				SDL_FreeSurface( pathUpDown ); 
				SDL_FreeSurface( pathUpRight ); 
				SDL_FreeSurface( screen ); 
				SDL_FreeSurface( instructions ); //Free the sound effects
				Mix_FreeChunk( shoot );
				Mix_FreeChunk( missile );
				Mix_FreeChunk( death );
				Mix_FreeChunk( ray );
				//Free the music
				Mix_FreeMusic( music3 );
			}
		}
		// _____________________________________MENU CODE END_________________________________
		makemountains(vec2D, screen);
		//____________MAIN RUNNING WHILE LOOP//_________________________________________________
		while (running == 1){
			while( SDL_PollEvent( &occur ) ){
				switch( occur.type ){
				case SDL_QUIT:
					runningMenu = 0;
					running = 0;
					runningWhole = 0;
					runningBestiary = 0;
					runningInstructions = 0;
					runningCredits = 0;
				case SDL_MOUSEBUTTONDOWN:
					switch( occur.button.button ){
						case SDL_BUTTON_LEFT:
							x = occur.button.x;
							y = occur.button.y;
							x = (x - x%25); // this just makes the x and y equal places in the vec2D
							y = (y - y%25);
							if(place_tow == 1){
								if(money_count >= firetowercost){
									if(checkforaddition(vec2D, (x - x%25)/25 , (y - y%25)/25) ) {
										money_count -= firetowercost;
										numberoffiretower ++;
										firetowercost = firetowercost + 25*numberoffiretower;
										AOETower *new_tower;
										new_tower = new AOETower;
										new_tower->settowerRectx(x);
										new_tower->settowerRecty(y);
										new_tower->setType(1);
										tower_vec.push_back(new_tower);
										if( Mix_PlayChannel( -1, death, 0 ) == -1 ) { return 1; }
									}
								}
							}
							if(place_tow == 2){
								if(money_count >= wizardtowercost){
									if(checkforaddition(vec2D, (x - x%25)/25 , (y - y%25)/25) ) {
										money_count -= wizardtowercost;
										numberofwizardtower ++;
										wizardtowercost = wizardtowercost + 25*numberofwizardtower;
										PULSTower *new_tower2;
										new_tower2 = new PULSTower;
										new_tower2->settowerRectx(x);
										new_tower2->settowerRecty(y);
										new_tower2->setType(2);
										tower_vec.push_back(new_tower2);
										if( Mix_PlayChannel( -1, death, 0 ) == -1 ) { return 1; }
									}
								}
							}
							if(place_tow == 3){
								if(money_count >= shottowercost){
									if(checkforaddition(vec2D, (x - x%25)/25 , (y - y%25)/25) ) {
									money_count -= shottowercost;
									numberofshottower++;
									shottowercost = shottowercost + 25*numberofshottower;
									SHOTTower *new_tower3;
									new_tower3 = new SHOTTower;
									new_tower3->settowerRectx(x);
									new_tower3->settowerRecty(y);
									new_tower3->setType(3);
									tower_vec.push_back(new_tower3);
									//delete new_tower3;
									//Play the death effect (which is just a place sounde effect 
									if( Mix_PlayChannel( -1, death, 0 ) == -1 ) { return 1; }
									}
								}
							}
							break;
						case SDL_BUTTON_RIGHT:
							x = occur.button.x;
							y = occur.button.y;
							place_tow=placetower(x,y);
							break;
							/* Look for a keypress */
				}
				case SDL_KEYDOWN:
					//int resume=0;
					/* Check the SDLKey values and move change the coords */
					if(occur.key.keysym.sym == SDLK_q){
						running = 0;
						runningWhole = 0;
					}
					//If 9 was pressed else
					if( occur.key.keysym.sym == SDLK_9 ) { 
						//If there is no music playing
						if( Mix_PlayingMusic() == 0 ) {
							//Play the music
							if( Mix_PlayMusic( music3, -1 ) == -1 ) {
							return 1;
							}
						} 	else { 
							//If music is being played
							//If the music is paused
							if( Mix_PausedMusic() == 1 ) {
								//Resume the music
								Mix_ResumeMusic();
							} 	else { 
								//If the music is playing 
								//Pause the music 
								Mix_PauseMusic();
							}
						}
					}
					if( occur.key.keysym.sym == SDLK_8 ) { 
						//If there is no music playing
						if( Mix_PlayingMusic() == 0 ) {
							//Play the music
							if( Mix_PlayMusic( music3, -1 ) == -1 ) {
								return 1;
							}
						} 	else { 
							//If music is being played
							//If the music is paused
							if( Mix_PausedMusic() == 1 ) {
								//Resume the music
								Mix_ResumeMusic();
							} else {
								//If the music is playing 
								//Pause the music 
								Mix_PauseMusic();
							}
						}
					}
					//If 0 was pressed
					if( occur.key.keysym.sym == SDLK_0 ) {
						//Stop the music
						Mix_HaltMusic();
					}
					switch( occur.key.keysym.sym ){
						case SDLK_SPACE:
							runningMenu = 1;
							running = 0;
							runningWhole = 1;
							runningBestiary = 0;
							runningInstructions = 0;
							runningCredits = 0;
							break;
						default:
							break;
					}
				}
			}
			setXsandYs(vec2D); // This set the x and y coordinates in the 2Dvec
			//ANIMATE!!
			// 
			for(int er = 0; er<35; er++){ // This code just animates 2Dvec
				for(int ter = 0; ter <25; ter++){
					treelocationRect.x = vec2D[er][ter].getxcoord();
					treelocationRect.y = vec2D[er][ter].getycoord();
					if(vec2D[er][ter].getimage() == 2){
						SDL_BlitSurface( midground, NULL, screen, &treelocationRect );
					}
					if(vec2D[er][ter].getimage() == 4){
						SDL_BlitSurface( ground, NULL, screen, &treelocationRect );
					}
					if(vec2D[er][ter].getimage() == 5){
						SDL_BlitSurface( pathRight, NULL, screen, &treelocationRect );
					}
					if(vec2D[er][ter].getimage() == 6){
						SDL_BlitSurface( pathLeftDown, NULL, screen, &treelocationRect );
					}
					if(vec2D[er][ter].getimage() == 7){
						SDL_BlitSurface( pathLeftUp, NULL, screen, &treelocationRect );
					}
						if(vec2D[er][ter].getimage() == 8){
					SDL_BlitSurface( pathDownRight, NULL, screen, &treelocationRect );
					}
						if(vec2D[er][ter].getimage() == 9){
					SDL_BlitSurface( pathUpRight, NULL, screen, &treelocationRect );
					}
						if(vec2D[er][ter].getimage() == 10){
					SDL_BlitSurface( pathUpDown, NULL, screen, &treelocationRect );
					}
					if(vec2D[er][ter].getimage() == 15){
						vec2D[er][ter].setxsize(2);
						vec2D[er][ter].setysize(2);
						vec2D[er+1][ter+1].setimage(NULL);
						vec2D[er+1][ter].setimage(NULL);
						vec2D[er][ter+1].setimage(NULL);
					}
					if(treescroll % 2 == 0){
						treelocationRect.x = vec2D[er][ter].getxcoord();
						treelocationRect.y = vec2D[er][ter].getycoord();
					}
					if(vec2D[er][ter].getimage() == 3 && vec2D[er][ter].getstat() == 0){
						vec2D[er][ter].setimage(11);
						SDL_BlitSurface( treeLeftish, NULL, screen, &treelocationRect );
						continue;
					}
					if(vec2D[er][ter].getimage() == 11 && vec2D[er][ter].getstat() == 0){
						vec2D[er][ter].setimage(12);
						SDL_BlitSurface( treeLeft, NULL, screen, &treelocationRect );
						vec2D[er][ter].setstat(1);
						continue;
					}
					if(vec2D[er][ter].getimage() == 12){
						vec2D[er][ter].setimage(11);
						SDL_BlitSurface( treeLeftish, NULL, screen, &treelocationRect );
						continue;
					}
					if(vec2D[er][ter].getimage() == 11 && vec2D[er][ter].getstat() == 1){
						vec2D[er][ter].setimage(3);
						SDL_BlitSurface( treeUp, NULL, screen, &treelocationRect );
						continue;
					}
					if(vec2D[er][ter].getimage() == 3 && vec2D[er][ter].getstat() == 1){
						vec2D[er][ter].setimage(13);
						SDL_BlitSurface( treeRightish, NULL, screen, &treelocationRect );
						continue;
					}
					if(vec2D[er][ter].getimage() == 13 && vec2D[er][ter].getstat() == 1){
						vec2D[er][ter].setimage(14);
						SDL_BlitSurface( treeRight, NULL, screen, &treelocationRect );
						vec2D[er][ter].setstat(0);
						continue;
					}
					if(vec2D[er][ter].getimage() == 14){
						vec2D[er][ter].setimage(13);
						SDL_BlitSurface( treeRightish, NULL, screen, &treelocationRect );
						continue;
					}
					if(vec2D[er][ter].getimage() == 13 && vec2D[er][ter].getstat() == 0){
						vec2D[er][ter].setimage(3);
						SDL_BlitSurface( treeUp, NULL, screen, &treelocationRect );
						continue;
					} 
				}
			}
			toweroptions(vec2D, screen); // these are displayed at y = 60 x = 710 ylength = 50 xlength  = 150 I am not worrying about filling those with occupied ground spaces because I would prefer leaving the backgrounds there. - MXS
			//Prints Money
			countingFunc(money_count, 50, 600, vec2D, screen); 
			printEnemyCount(Enemies_Left, 700, 600, vec2D, screen, 1 ); 
			printEnemyCount(Lives, 300, 600, vec2D, screen, 0 ); 
			//begin draw enemies
			int delay=0;
			int pic=1;
			if(control < enemy_vec.size()){  
				en = enemy_vec.begin();   
				for(int q=0; q<control; q++){
					if((en->getArmour() <= 0) && ( en->getType() != 5 )){ 
						en->setType(5); 
						en->drawEnemy(5);
						Enemies_Left--;
						money_count += en-> getReward();
						//Play the missile effect (which is just a click sound effect 
						if( Mix_PlayChannel( -1, missile, 0 ) == -1 ) { return 1; }
					}
					en->animate(screen, i, treescroll,q);
					en++;
					if(en == enemy_vec.end() ) en = enemy_vec.begin();
				}
			} else{
				for(en = enemy_vec.begin(); en != enemy_vec.end(); en++){
					if( (en->getguyRect().x == 850) && ( en->getType() != 5 ) ){ 
						Enemies_Left--;  
						en->setType(5); 
						en -> drawEnemy( 5 );
						Lives--;
					}
					if((en->getArmour() <= 0) && ( en->getType() != 5 )){ 
						en->setType(5); 
						en->drawEnemy(5);
						Enemies_Left--;
						money_count += en-> getReward();
						//Play the missile effect (which is just a click sound effect 
						if( Mix_PlayChannel( -1, missile, 0 ) == -1 ) { return 1; }
					}
					en->animate(screen, i, treescroll, delay);
					delay ++;
				}
			}
			//end drawing enemies
			if (!enemy_vec.empty() ){
				if( Enemies_Left <= 0 ){ 
					delete_enemies( enemy_vec );
					wave++;
					cout<<" this is before create enemies"<<endl<< "the number of waves is: "<<wave<<endl;
					create_enemies(wave, enemy_vec);
					i=path_vec.begin();
					for (em = enemy_vec.begin() ; em != enemy_vec.end() ; em ++ ){
						if( em->getType() != 5) Enemies_Left ++;
					}
					control=0;
				}
			}
			if(treescroll%2 ==0){ i++; control++; }
			int enemy_selector = 0;
			for( j = tower_vec.begin(); j != tower_vec.end(); j++) {
				(*j)->animate( screen );
			}
			for( j = tower_vec.begin(); j != tower_vec.end(); j++){
				(*j)->Attack( enemy_vec, screen, treescroll );
			}
			//TREE ANIMATION
			treescroll++;
			//changetrees(vec2D, treescroll, screen);
			screen = printtowercost(wizardtowercost, 25, 50, vec2D, screen);
			screen = printtowercost(firetowercost, 200, 50, vec2D, screen);
			screen = printtowercost(shottowercost, 375, 50, vec2D, screen);
			//Update Screen
			SDL_Flip( screen );
			// Pause
			SDL_Delay( 300 );
			if( Lives <= 0 ){ // THIS MEANS THE PLAYER LOST THE GAME
				running = 0;
				runningWhole = 0;
				SDL_BlitSurface( lose, NULL, screen, &menuRect );
				SDL_Flip( screen );
				SDL_Delay( 2000 );
			}
			if( wave > 10){ // THIS MEANS THE PLAYER WON THE GAME
				running = 0;
				runningWhole = 0;
				SDL_BlitSurface( win, NULL, screen, &menuRect );
				SDL_Flip( screen );
				SDL_Delay( 2000 );
			}
			if (runningWhole == 0){    // just freeing alot of things that need freeing
				runningMenu = 0;
				running = 0;
				runningBestiary = 0;
				runningInstructions = 0;
				runningCredits = 0;
				SDL_FreeSurface( lose ); 
				SDL_FreeSurface( win ); 
				SDL_FreeSurface( dome ); 
				SDL_FreeSurface( bestiary ); 
				SDL_FreeSurface( credits ); 
				SDL_FreeSurface( ground ); 
				SDL_FreeSurface( midground ); 
				SDL_FreeSurface( picture );
				SDL_FreeSurface( menu );
				SDL_FreeSurface( occupiedGround ); 
				SDL_FreeSurface( pathRight ); 
				SDL_FreeSurface( pathDownRight ); 
				SDL_FreeSurface( pathLeftDown ); 
				SDL_FreeSurface( pathLeftUp ); 
				SDL_FreeSurface( pathUpDown ); 
				SDL_FreeSurface( pathUpRight ); 
				SDL_FreeSurface( screen ); 
				SDL_FreeSurface( instructions );
				SDL_FreeSurface( treeUp );
				SDL_FreeSurface( treeLeft );
				SDL_FreeSurface( treeLeftish );
				SDL_FreeSurface( treeRight );
				SDL_FreeSurface( treeRightish );
				//Free the sound effects
				Mix_FreeChunk( shoot );
				Mix_FreeChunk( missile );
				Mix_FreeChunk( death );
				Mix_FreeChunk( ray );
				//Free the music
				Mix_FreeMusic( music3 );
			}

		} // THIS CLOSES RUNNING
	} 
	for(j = tower_vec.begin(); j != tower_vec.end(); j++){
		(*j)-> freeTower();
	} 

	for( int m=0; m<tower_vec.size(); m++){
		delete tower_vec[m];
	}

	Mix_CloseAudio();
	SDL_Quit();
}//end main


