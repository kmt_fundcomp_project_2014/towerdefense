#include <vector>
#include "griddy.h"
#include "SDL/SDL.h"
#include <SDL/SDL_image.h>
#include "tdfunctions.h"
#include <stdio.h>
#include <sstream>
#include <fstream>
#include "Enemy.h"
#include <string>



int delete_enemies( vector< Enemy > &enemy_vec ){

// THIS ERASES ENEMIES
	int deletedenemies = 0;
	while ( !enemy_vec.empty() ){
		enemy_vec.pop_back();
	}
	return deletedenemies;
}


void create_enemies( int wave, vector< Enemy > &enemy_vec){
	string filename;
	Enemy new_enemy;
	if (wave == 1){
		filename="enemies1.txt";   // This code is kinda self-explanatory this 
	} else if (wave == 2){             //takes .txt files and fills enemy_vec with enemies based on the files  
		filename="enemies2.txt";
	} else if (wave == 3){
		filename="enemies3.txt";
	} else if (wave ==4){
		filename="enemies4.txt";
	} else if (wave ==5){
		filename="enemies5.txt";
	} else if (wave ==6){
		filename="enemies6.txt";
	} else if (wave ==7){
		filename="enemies7.txt";
	} else if (wave ==8){
		filename="enemies8.txt";
	} else if (wave ==9){
		filename="enemies9.txt";
	} else {
		filename="enemies10.txt";
	}
	ifstream boardFile;    //makes a ifstream object in order to open file
	boardFile.open (filename.c_str(), ios::in);//open file to make it readable
	if( !boardFile){	//checks to make sure file was opened
		cout<<"File could not be opened! "<<endl;
	} else{	//else - file was opened successfully
		while( !boardFile.eof() ){//Until the end of the file (EOF) is reached
			string line;//will contain the line as a whole string
			int value;//sets value of type T
			while( getline(boardFile, line) ){//while there are still lines in boardfile to assigned to line variable
				vector<int> row;
				stringstream splitting(line);//Puts the line into splitting
				for( int i=0; i < 2; i++){
					splitting >> value; //This outputs each item of splitting delimiting spaces into value
					row.push_back(value);//Puts all the values in the line into the newLine row by placing them at the end
				}
				new_enemy.setType(row[0]);
				enemy_vec.push_back(new_enemy);
			}	//Thus creating a 2D vector
		}
		boardFile.close();//close file
	}								
}

SDL_Surface* toweroptions(vector<vector<griddy> > &vec2D, SDL_Surface* screen){
	SDL_Surface* FireTowerChoice = NULL;      // THIS DISPLAYS THE tower options (towers with gray backgrounds) to choose what  
	SDL_Surface* WizardTowerChoice = NULL;    // type of tower you want to place in the top left part of the screen which you right click
	SDL_Surface* WaterTowerChoice = NULL;
	SDL_Rect FireOptionLocationRect;
	SDL_Rect WizardOptionLocationRect;
	SDL_Rect WaterOptionLocationRect;
	FireOptionLocationRect.x = 230;
	FireOptionLocationRect.y = 80;
	WizardOptionLocationRect.x = 55;
	WizardOptionLocationRect.y = 80;
	WaterOptionLocationRect.x = 400;
	WaterOptionLocationRect.y = 80;
	FireTowerChoice = IMG_Load("FireTowerChoice.png");
	WizardTowerChoice = IMG_Load("WizardTowerChoice.png");
	WaterTowerChoice = IMG_Load("WaterTowerChoice.png");
	SDL_BlitSurface(FireTowerChoice, NULL, screen, &FireOptionLocationRect);
	SDL_BlitSurface(WizardTowerChoice, NULL, screen, &WizardOptionLocationRect);
	SDL_BlitSurface(WaterTowerChoice, NULL, screen, &WaterOptionLocationRect);
	SDL_FreeSurface( FireTowerChoice );
	SDL_FreeSurface( WizardTowerChoice );
	SDL_FreeSurface( WaterTowerChoice );
	return screen;
}
	

SDL_Surface* makemountains(vector<vector<griddy> > &vec2D, SDL_Surface* screen){
  // MAKING MOUNTAINS
  SDL_Rect mountainRect;
  mountainRect.x = 0;
  mountainRect.y = 0;
  SDL_Surface* mountain = NULL; 
  mountain = SDL_LoadBMP ("mountain.bmp" );//1
  for( int we = 0; we < 35; we+=3){ // This is since the mountains are along the x-axis and are of xsize = 3
 	vec2D[we][0].setimage(17); // the first image occupies [0][0] -  [2][2]/ the top left is now occupied gorund the other 8 are NULL
 	vec2D[we][0].setxsize(3);
 	vec2D[we][0].setysize(3);
	mountainRect.x = 25*we ;
 	vec2D[15][15].setimage(4); //this fixed a stupid glitch and im not sure why
 	vec2D[15][15].setxsize(1);
 	vec2D[15][15].setysize(1);
	SDL_BlitSurface( mountain, NULL, screen, &mountainRect );
  }
  SDL_FreeSurface( mountain );
  return screen;
}



int placetower(int x, int y){ // This function checks the location of something and returns a number based on the locaiton of the click
	if(x > 230 && x < 285 && y >80 && y < 135){
		return 1;
	}
	else if(x > 55 && x < 110 && y >80 && y < 135){
		return 2;
	}
	else if(x > 400 && x < 455 && y >80 && y < 135){
		return 3;
	}
	else{
		return 0;
	}
}


void makemidground(vector<vector<griddy> > &vec2D){ // This sets the 3rd row in vec2D to be filled with midground
// Making Midgrounds
 for(int re = 0; re < 35; re+=2){
	vec2D[re][3].setimage(2);
	vec2D[re][3].setxsize(2);
	vec2D[re][3].setysize(2);
 }
}

// Setting NULLs for covered ground squares
void makeNULLS(vector<vector<griddy> > &vec2D){ // This sets the image to null for all the spaces which are grass but... 
//should be covered by other images. This includes like if vec[0][0]=mountain then vec[0][1] should be NUll but right now its ground
// without this function 7/8ths of the moutain image would be blitted over with ground
for( int rt = 0; rt < 35; rt++){ // This is just setting all the squares to ground
	for (int qw = 0; qw < 25; qw++){
		if((vec2D[rt][qw].getxsize() == 2 || vec2D[rt][qw].getxsize() == 3) && rt < 34)
		{
			vec2D[rt+1][qw].setimage(NULL);
		}
		// if image has a y size of 2 or 3 then free the image below it
		if((vec2D[rt][qw].getysize() == 2 || vec2D[rt][qw].getysize() == 3) && qw < 24)
		{
			vec2D[rt][qw+1].setimage(NULL);
		} 

		// if an image is a 2x2 or 2x3 or 3x3 then free the image in the square down right diag to it
		if((vec2D[rt][qw].getysize() >= 2 ) && (vec2D[rt][qw].getxsize() >= 2) && qw < 24 && rt < 34)
		{
			vec2D[rt+1][qw+1].setimage(NULL);
		} 

		if(vec2D[rt][qw].getxsize() == 3 && rt < 33)
		{
			vec2D[rt+2][qw].setimage(NULL);
		}

		if(vec2D[rt][qw].getysize() == 3 && qw < 23)
		{
			vec2D[rt][qw+2].setimage(NULL);
		}

		if(vec2D[rt][qw].getxsize() == 3 && rt < 33 && qw<24 && (vec2D[rt][qw].getysize() == 2 || vec2D[rt][qw].getysize() == 3))
		{
			vec2D[rt+2][qw+1].setimage(NULL);
		}
		if(vec2D[rt][qw].getysize() == 3 && rt < 34 && qw<23 && (vec2D[rt][qw].getxsize() == 2 || vec2D[rt][qw].getxsize() == 3))
		{
			vec2D[rt+1][qw+2].setimage(NULL);
		}

		if(vec2D[rt][qw].getysize() == 3 && qw < 23)
		{
			vec2D[rt][qw+2].setimage(NULL);
		}

		if(vec2D[rt][qw].getysize() == 3 && qw < 23 && vec2D[rt][qw].getxsize() == 3 && rt < 33)
		{
			vec2D[rt+2][qw+2].setimage(NULL);
		}
	}
    }
}





SDL_Surface* changetrees(vector<vector<griddy> > &vec2D, int treescroll, SDL_Surface* screen){
   // This function checks the type of the tree and like if its far left then it changes it to slightly left
   // this function makes the trees wave in the wind
  SDL_Surface* treeUp = NULL;
  SDL_Surface* treeLeft = NULL;
  SDL_Surface* treeLeftish = NULL;
  SDL_Surface* treeRight = NULL;
  SDL_Surface* treeRightish = NULL;
  treeUp = SDL_LoadBMP( "treeUp.bmp"); //3
  treeLeftish = SDL_LoadBMP( "treeLeftish.bmp"); //11
  treeLeft = SDL_LoadBMP( "treeLeft.bmp");//12
  treeRightish = SDL_LoadBMP( "treeRightish.bmp");//13
  treeRight = SDL_LoadBMP( "treeRight.bmp");//14
  SDL_Rect treelocationRect;

  //When a tree is in the center sometimes it should proceed to move left and sometimes right 
  // The stat of the tree keeps track of which direction it should go

    if(treescroll % 2 == 0){
	for(int u = 0; u < 25; u++){
		for(int yu = 0; yu < 35; yu++){
 			treelocationRect.x = vec2D[yu][u].getxcoord();
			treelocationRect.y = vec2D[yu][u].getycoord();
		
	  		if(vec2D[yu][u].getimage() == 3 && vec2D[yu][u].getstat() == 0){
				vec2D[yu][u].setimage(11);
				SDL_BlitSurface( treeLeftish, NULL, screen, &treelocationRect );
				continue;
	  		}
	  		if(vec2D[yu][u].getimage() == 11 && vec2D[yu][u].getstat() == 0){
				vec2D[yu][u].setimage(12);
				SDL_BlitSurface( treeLeft, NULL, screen, &treelocationRect );
				vec2D[yu][u].setstat(1);
				continue;
	  		}
	  		if(vec2D[yu][u].getimage() == 12){ // if the tree is far left change it to slightly left
				vec2D[yu][u].setimage(11);
				SDL_BlitSurface( treeLeftish, NULL, screen, &treelocationRect );
				continue;
	  		}
	  		if(vec2D[yu][u].getimage() == 11 && vec2D[yu][u].getstat() == 1){
				vec2D[yu][u].setimage(3);
				SDL_BlitSurface( treeUp, NULL, screen, &treelocationRect );
				continue;
	  		}
	  		if(vec2D[yu][u].getimage() == 3 && vec2D[yu][u].getstat() == 1){
				vec2D[yu][u].setimage(13);
				SDL_BlitSurface( treeRightish, NULL, screen, &treelocationRect );
				continue;
	  		}
	  		if(vec2D[yu][u].getimage() == 13 && vec2D[yu][u].getstat() == 1){
				vec2D[yu][u].setimage(14);
				SDL_BlitSurface( treeRight, NULL, screen, &treelocationRect );
				vec2D[yu][u].setstat(0);
				continue;
	  		}
	  		if(vec2D[yu][u].getimage() == 14){ // if the tree is far right change it to slighty right
				vec2D[yu][u].setimage(13);
				SDL_BlitSurface( treeRightish, NULL, screen, &treelocationRect );
				continue;
	  		}
	  		if(vec2D[yu][u].getimage() == 13 && vec2D[yu][u].getstat() == 0){
				vec2D[yu][u].setimage(3);
				SDL_BlitSurface( treeUp, NULL, screen, &treelocationRect );
				continue;
	  		}
		}
	}
    } // end of treescroll if statement
  SDL_FreeSurface( treeLeftish );
  SDL_FreeSurface( treeLeft );
  SDL_FreeSurface( treeUp );
  SDL_FreeSurface( treeRightish );
  SDL_FreeSurface( treeRight );
return screen;
}


void setXsandYs(vector<vector<griddy> > &vec2D){ // This sets the xcoord and ycoord of all the griddys in 
						 //vec2D to 25*row = xcoord and 25*column=ycoord
						 //Setting x and y coords for the Griddy's in Vec2D
  for(int ypi = 0; ypi<35; ypi++){
	for(int yui = 0; yui <25; yui++){
		vec2D[ypi][yui].setxcoord(ypi*25);
		vec2D[ypi][yui].setycoord(yui*25);
	}
  }
}

void makegrounddefault(vector<vector<griddy> > &vec2D){ // This fills all of vec2D with ground tiles initially
  // Making Ground the default
  for( int fe = 0; fe < 35; fe++){ // This is just setting all the squares to ground
	for (int xs = 0; xs < 25; xs++){
		vec2D[fe][xs].setimage(4); 
		vec2D[fe][xs].setxsize(1);
		vec2D[fe][xs].setysize(1);
		vec2D[fe][xs].setstat(0);
	}
  }
}

int checkforaddition(vector<vector<griddy> > &vec2D, int x, int y){ // This checks if a tower can fit in the spot where the user is clicking
	if(vec2D[x][y].getimage() != 4){ 
		return 0;
	}
	if(vec2D[x+1][y].getimage() != 4){
		return 0;
	}
	if(vec2D[x][y+1].getimage() != 4){
		return 0;
	}
	if(vec2D[x+1][y+1].getimage() != 4){
		return 0;
	}
	vec2D[x][y].setimage(17); // if it does fit it fills that spot in vec2D with occupied ground
	vec2D[x][y+1].setimage(17); // there are four because towers are 2-by-2 in vec2D 
	vec2D[x+1][y].setimage(17);
	vec2D[x+1][y+1].setimage(17);
return 1;
}

SDL_Surface* countingFunc(int number, int x, int y, vector<vector<griddy> > &vec2D, SDL_Surface* screen){

// This function is pretty simple it just blits a number on the screen with a dollar sign to the left
// it also sets the area in vec2D which it will be blitting over with occupied ground 
// this one has 5 digits 

 SDL_Rect numberlocationRect;

 SDL_Surface* dollarsign = NULL;
 SDL_Surface* zero = NULL;
 SDL_Surface* one = NULL;
 SDL_Surface* two = NULL;
 SDL_Surface* three = NULL;
 SDL_Surface* four = NULL;
 SDL_Surface* five = NULL;
 SDL_Surface* six = NULL;
 SDL_Surface* seven = NULL;
 SDL_Surface* eight = NULL;
 SDL_Surface* nine = NULL;
 dollarsign = IMG_Load ( "dollarsign.png" );
 one = IMG_Load( "one.png" ); 
 two = IMG_Load( "two.png" ); 
 three = IMG_Load( "three.png" ); 
 four = IMG_Load( "four.png" ); 	
 five = IMG_Load( "five.png" ); 
 six = IMG_Load( "six.png" ); 
 seven = IMG_Load( "seven.png" ); 
 eight = IMG_Load( "eight.png" ); 
 nine = IMG_Load( "nine.png" ); 
 zero = IMG_Load( "zero.png" );

 numberlocationRect.x = x-25;
 numberlocationRect.y = y;

	SDL_BlitSurface( dollarsign, NULL, screen, &numberlocationRect );

	if(number%10 == 0)
	{
		numberlocationRect.x = x+90;
		numberlocationRect.y = y;
		SDL_BlitSurface( zero, NULL, screen, &numberlocationRect ); 
	}
	if(number%10 == 1)
	{
		numberlocationRect.x = x+90;
		numberlocationRect.y = y;
		SDL_BlitSurface( one, NULL, screen, &numberlocationRect ); 
	}
	if(number%10 == 2)
	{
		numberlocationRect.x = x+90;
		numberlocationRect.y = y;
		SDL_BlitSurface( two, NULL, screen, &numberlocationRect );
	}
	if(number%10 == 3)
	{		
		numberlocationRect.x = x+90;
		numberlocationRect.y = y;
		SDL_BlitSurface( three, NULL, screen, &numberlocationRect ); 
	}
	if(number%10 == 4)
	{
		numberlocationRect.x = x+90;
		numberlocationRect.y = y;
		SDL_BlitSurface( four, NULL, screen, &numberlocationRect ); 
	}
	if(number%10 == 5)
	{
		numberlocationRect.x = x+90;
		numberlocationRect.y = y;
		SDL_BlitSurface( five, NULL, screen, &numberlocationRect ); 
	}
	if(number%10 == 6)
	{
		numberlocationRect.x = x+90;
		numberlocationRect.y = y;
		SDL_BlitSurface( six, NULL, screen, &numberlocationRect ); 
	}
	if(number%10 == 7)
	{	
		numberlocationRect.x = x+90;
		numberlocationRect.y = y;
		SDL_BlitSurface( seven, NULL, screen, &numberlocationRect ); 
	}
	if(number%10 == 8)
	{
		numberlocationRect.x = x+90;
		numberlocationRect.y = y;
		SDL_BlitSurface( eight, NULL, screen, &numberlocationRect ); 
	}
	if(number%10 == 9)
	{	
		numberlocationRect.x = x+90;
		numberlocationRect.y = y;
		SDL_BlitSurface( nine, NULL, screen, &numberlocationRect ); 
	}


	if((number/10)%10 == 0)
	{

		numberlocationRect.x = x+67;
		numberlocationRect.y = y;
		SDL_BlitSurface( zero, NULL, screen, &numberlocationRect ); 
		}
	if((number/10)%10 == 1)
	{
		numberlocationRect.x = x+67;
		numberlocationRect.y = y;
		SDL_BlitSurface( one, NULL, screen, &numberlocationRect ); 
	}
	if((number/10)%10 == 2)
	{
		numberlocationRect.x = x+67;
		numberlocationRect.y = y;
		SDL_BlitSurface( two, NULL, screen, &numberlocationRect ); 
	}
	if((number/10)%10 == 3)
	{
		numberlocationRect.x = x+67;
		numberlocationRect.y = y;
		SDL_BlitSurface( three, NULL, screen, &numberlocationRect ); 
	}
	if((number/10)%10 == 4)
	{
		numberlocationRect.x = x+67;
		numberlocationRect.y = y;
		SDL_BlitSurface( four, NULL, screen, &numberlocationRect ); 
	}
	if((number/10)%10 == 5)
	{
		numberlocationRect.x = x+67;
		numberlocationRect.y = y;
		SDL_BlitSurface( five, NULL, screen, &numberlocationRect );
	}
	if((number/10)%10 == 6)
	{
		numberlocationRect.x = x+67;
		numberlocationRect.y = y;
		SDL_BlitSurface( six, NULL, screen, &numberlocationRect ); 
	}
	if((number/10)%10 == 7)
	{
		numberlocationRect.x = x+67;
		numberlocationRect.y = y;
		SDL_BlitSurface( seven, NULL, screen, &numberlocationRect ); 
	}
	if((number/10)%10 == 8)
	{
		numberlocationRect.x = x+67;
		numberlocationRect.y = y;
		SDL_BlitSurface( eight, NULL, screen, &numberlocationRect ); 
	}
	if((number/10)%10 == 9)
	{
		numberlocationRect.x = x+67;
		numberlocationRect.y = y;
		SDL_BlitSurface( nine, NULL, screen, &numberlocationRect ); 
	}

	if((number/100)%10 == 0)
	{

		numberlocationRect.x = x+45;
		numberlocationRect.y = y;
		SDL_BlitSurface( zero, NULL, screen, &numberlocationRect ); 
	}
	if((number/100)%10 == 1)
	{
		numberlocationRect.x = x+45;
		numberlocationRect.y = y;
		SDL_BlitSurface( one, NULL, screen, &numberlocationRect ); 
	}
	if((number/100)%10 == 2)
	{
		numberlocationRect.x = x+45;
		numberlocationRect.y = y;
		SDL_BlitSurface( two, NULL, screen, &numberlocationRect ); 
	}
	if((number/100)%10 == 3)
	{
		numberlocationRect.x = x+45;
		numberlocationRect.y = y;
		SDL_BlitSurface( three, NULL, screen, &numberlocationRect ); 
	}
	if((number/100)%10 == 4)
	{
		numberlocationRect.x = x+45;
		numberlocationRect.y = y;
		SDL_BlitSurface( four, NULL, screen, &numberlocationRect ); 
	}
	if((number/100)%10 == 5)
	{
		numberlocationRect.x = x+45;
		numberlocationRect.y = y;
		SDL_BlitSurface( five, NULL, screen, &numberlocationRect ); 
	}
	if((number/100)%10 == 6)
	{
		numberlocationRect.x = x+45;
		numberlocationRect.y = y;
		SDL_BlitSurface( six, NULL, screen, &numberlocationRect ); 
	}
	if((number/100)%10 == 7)
	{
		numberlocationRect.x = x+45;
		numberlocationRect.y = y;
		SDL_BlitSurface( seven, NULL, screen, &numberlocationRect ); 
	}
	if((number/100)%10 == 8)
	{
		numberlocationRect.x = x+45;
		numberlocationRect.y = y;
		SDL_BlitSurface( eight, NULL, screen, &numberlocationRect ); 
	}
	if((number/100)%10 == 9)
	{
		numberlocationRect.x = x+45;
		numberlocationRect.y = y;
		SDL_BlitSurface( nine, NULL, screen, &numberlocationRect ); 
	}
	if((number/1000)%10 == 0)
	{
		numberlocationRect.x = x+22;
		numberlocationRect.y = y;
		SDL_BlitSurface( zero, NULL, screen, &numberlocationRect ); 
	}
	if((number/1000)%10 == 1)
	{
		numberlocationRect.x = x+22;
		numberlocationRect.y = y;
		SDL_BlitSurface( one, NULL, screen, &numberlocationRect ); 
	}
	if((number/1000)%10 == 2)
	{
		numberlocationRect.x = x+22;
		numberlocationRect.y = y;
		SDL_BlitSurface( two, NULL, screen, &numberlocationRect ); 
	}
	if((number/1000)%10 == 3)
	{
		numberlocationRect.x = x+22;
		numberlocationRect.y = y;
		SDL_BlitSurface( three, NULL, screen, &numberlocationRect ); 
	}
	if((number/1000)%10 == 4)
	{
		numberlocationRect.x = x+22;
		numberlocationRect.y = y;
		SDL_BlitSurface( four, NULL, screen, &numberlocationRect ); 
	}
	if((number/1000)%10 == 5)
	{
		numberlocationRect.x = x+22;
		numberlocationRect.y = y;
		SDL_BlitSurface( five, NULL, screen, &numberlocationRect ); 
	}
	if((number/1000)%10 == 6)
	{
		numberlocationRect.x = x+22;
		numberlocationRect.y = y;
		SDL_BlitSurface( six, NULL, screen, &numberlocationRect ); 
	}
	if((number/1000)%10 == 7)
	{
		numberlocationRect.x = x+22;
		numberlocationRect.y = y;
		SDL_BlitSurface( seven, NULL, screen, &numberlocationRect ); 
	}
	if((number/1000)%10 == 8)
	{
		numberlocationRect.x = x+22;
		numberlocationRect.y = y;
		SDL_BlitSurface( eight, NULL, screen, &numberlocationRect );
	}
	if((number/1000)%10 == 9)
	{
		numberlocationRect.x = x+22;
		numberlocationRect.y = y;
		SDL_BlitSurface( nine, NULL, screen, &numberlocationRect ); 
	}


	if((number/10000) == 0)
	{

		numberlocationRect.x = x;
		numberlocationRect.y = y;
		SDL_BlitSurface( zero, NULL, screen, &numberlocationRect ); 
	}
	if((number/10000) == 1)
	{
		numberlocationRect.x = x;
		numberlocationRect.y = y;
		SDL_BlitSurface( one, NULL, screen, &numberlocationRect );
	}
	if((number/10000) == 2)
	{
		numberlocationRect.x = x;
		numberlocationRect.y = y;
		SDL_BlitSurface( two, NULL, screen, &numberlocationRect );
	}
	if((number/10000) == 3)
	{
		numberlocationRect.x = x;
		numberlocationRect.y = y;
		SDL_BlitSurface( three, NULL, screen, &numberlocationRect );
	}
	if((number/10000) == 4)
	{
		numberlocationRect.x = x;
		numberlocationRect.y = y;
		SDL_BlitSurface( four, NULL, screen, &numberlocationRect );
	}
	if((number/10000) == 5)
	{
		numberlocationRect.x = x;
		numberlocationRect.y = y;
		SDL_BlitSurface( five, NULL, screen, &numberlocationRect );
	}
	if((number/10000) == 6)
	{
		numberlocationRect.x = x;
		numberlocationRect.y = y;
		SDL_BlitSurface( six, NULL, screen, &numberlocationRect );
	}
	if((number/10000) == 7)
	{
		numberlocationRect.x = x;
		numberlocationRect.y = y;
		SDL_BlitSurface( seven, NULL, screen, &numberlocationRect );
	}
	if((number/10000) == 8)
	{
		numberlocationRect.x = x;
		numberlocationRect.y = y;
		SDL_BlitSurface( eight, NULL, screen, &numberlocationRect );
	}
	if((number/10000) == 9)
	{
		numberlocationRect.x = x;
		numberlocationRect.y = y;
		SDL_BlitSurface( nine, NULL, screen, &numberlocationRect );
	}

 	SDL_FreeSurface( dollarsign);
	SDL_FreeSurface( zero);
 	SDL_FreeSurface( one);
	SDL_FreeSurface( two); 	
	SDL_FreeSurface( three);
	SDL_FreeSurface( four); 	
	SDL_FreeSurface( five);
	SDL_FreeSurface( six); 	
	SDL_FreeSurface( seven);
	SDL_FreeSurface( eight); 	
	SDL_FreeSurface( nine);
return screen;
}



SDL_Surface* printEnemyCount(int number, int x, int y, vector<vector<griddy> > &vec2D, SDL_Surface* screen, int which){
// This function is pretty simple it just blits a number on the screen 
// This one specifically also outputs the text lives in front of number if that is what the user wants
//if which == 0 then the screen blits the texts lives 
// This one has 2 digits

 SDL_Surface* dollarsign = NULL;
 SDL_Surface* zero = NULL;
 SDL_Surface* one = NULL;
 SDL_Surface* two = NULL;
 SDL_Surface* three = NULL;
 SDL_Surface* four = NULL;
 SDL_Surface* five = NULL;
 SDL_Surface* six = NULL;
 SDL_Surface* seven = NULL;
 SDL_Surface* eight = NULL;
 SDL_Surface* nine = NULL;
 if (which) dollarsign = IMG_Load ( "enleft.png" );
 else dollarsign = IMG_Load ( "Lives.png");
 one = IMG_Load( "one.png" ); 
 two = IMG_Load( "two.png" ); 
 three = IMG_Load( "three.png" ); 
 four = IMG_Load( "four.png" ); 	
 five = IMG_Load( "five.png" ); 
 six = IMG_Load( "six.png" ); 
 seven = IMG_Load( "seven.png" ); 
 eight = IMG_Load( "eight.png" ); 
 nine = IMG_Load( "nine.png" ); 
 zero = IMG_Load( "zero.png" );
 SDL_Rect numberlocationRect;


	if (which) numberlocationRect.x = x-150;
	else numberlocationRect.x = x-30;
	numberlocationRect.y = y - 10;
	SDL_BlitSurface( dollarsign, NULL, screen, &numberlocationRect );

	if(number%10 == 0)
	{
		numberlocationRect.x = x+90;
		numberlocationRect.y = y;
		SDL_BlitSurface( zero, NULL, screen, &numberlocationRect ); 
	}
	if(number%10 == 1)
	{
		numberlocationRect.x = x+90;
		numberlocationRect.y = y;
		SDL_BlitSurface( one, NULL, screen, &numberlocationRect ); 
	}
	if(number%10 == 2)
	{
		numberlocationRect.x = x+90;
		numberlocationRect.y = y;
		SDL_BlitSurface( two, NULL, screen, &numberlocationRect );
	}
	if(number%10 == 3)
	{		
		numberlocationRect.x = x+90;
		numberlocationRect.y = y;
		SDL_BlitSurface( three, NULL, screen, &numberlocationRect ); 
	}
	if(number%10 == 4)
	{
		numberlocationRect.x = x+90;
		numberlocationRect.y = y;
		SDL_BlitSurface( four, NULL, screen, &numberlocationRect ); 
	}
	if(number%10 == 5)
	{
		numberlocationRect.x = x+90;
		numberlocationRect.y = y;
		SDL_BlitSurface( five, NULL, screen, &numberlocationRect ); 
	}
	if(number%10 == 6)
	{
		numberlocationRect.x = x+90;
		numberlocationRect.y = y;
		SDL_BlitSurface( six, NULL, screen, &numberlocationRect ); 
	}
	if(number%10 == 7)
	{	
		numberlocationRect.x = x+90;
		numberlocationRect.y = y;
		SDL_BlitSurface( seven, NULL, screen, &numberlocationRect ); 
	}
	if(number%10 == 8)
	{
		numberlocationRect.x = x+90;
		numberlocationRect.y = y;
		SDL_BlitSurface( eight, NULL, screen, &numberlocationRect ); 
	}
	if(number%10 == 9)
	{	
		numberlocationRect.x = x+90;
		numberlocationRect.y = y;
		SDL_BlitSurface( nine, NULL, screen, &numberlocationRect ); 
	}


	if((number/10)%10 == 0)
	{

		numberlocationRect.x = x+67;
		numberlocationRect.y = y;
		SDL_BlitSurface( zero, NULL, screen, &numberlocationRect ); 
		}
	if((number/10)%10 == 1)
	{
		numberlocationRect.x = x+67;
		numberlocationRect.y = y;
		SDL_BlitSurface( one, NULL, screen, &numberlocationRect ); 
	}
	if((number/10)%10 == 2)
	{
		numberlocationRect.x = x+67;
		numberlocationRect.y = y;
		SDL_BlitSurface( two, NULL, screen, &numberlocationRect ); 
	}
	if((number/10)%10 == 3)
	{
		numberlocationRect.x = x+67;
		numberlocationRect.y = y;
		SDL_BlitSurface( three, NULL, screen, &numberlocationRect ); 
	}
	if((number/10)%10 == 4)
	{
		numberlocationRect.x = x+67;
		numberlocationRect.y = y;
		SDL_BlitSurface( four, NULL, screen, &numberlocationRect ); 
	}
	if((number/10)%10 == 5)
	{
		numberlocationRect.x = x+67;
		numberlocationRect.y = y;
		SDL_BlitSurface( five, NULL, screen, &numberlocationRect );
	}
	if((number/10)%10 == 6)
	{
		numberlocationRect.x = x+67;
		numberlocationRect.y = y;
		SDL_BlitSurface( six, NULL, screen, &numberlocationRect ); 
	}
	if((number/10)%10 == 7)
	{
		numberlocationRect.x = x+67;
		numberlocationRect.y = y;
		SDL_BlitSurface( seven , NULL, screen, &numberlocationRect ); 
	}
	if((number/10)%10 == 8)
	{
		numberlocationRect.x = x+67;
		numberlocationRect.y = y;
		SDL_BlitSurface( eight, NULL, screen, &numberlocationRect ); 
	}
	if((number/10)%10 == 9)
	{
		numberlocationRect.x = x+67;
		numberlocationRect.y = y;
		SDL_BlitSurface( nine, NULL, screen, &numberlocationRect ); 
	}

 	SDL_FreeSurface( dollarsign);
	SDL_FreeSurface( zero);
 	SDL_FreeSurface( one);
	SDL_FreeSurface( two); 	
	SDL_FreeSurface( three);
	SDL_FreeSurface( four); 	
	SDL_FreeSurface( five);
	SDL_FreeSurface( six); 	
	SDL_FreeSurface( seven);
	SDL_FreeSurface( eight); 	
	SDL_FreeSurface( nine);
	return screen;
}

SDL_Surface* printtowercost(int number, int x, int y, vector<vector<griddy> > &vec2D, SDL_Surface* screen){
	//very similar to the other counting funcitons this one has 4 digits
 	SDL_Surface* dollarsign = NULL;
 	SDL_Surface* zero = NULL;
 	SDL_Surface* one = NULL;
 	SDL_Surface* two = NULL;
 	SDL_Surface* three = NULL;
 	SDL_Surface* four = NULL;
 	SDL_Surface* five = NULL;
 	SDL_Surface* six = NULL;
 	SDL_Surface* seven = NULL;
 	SDL_Surface* eight = NULL;
 	SDL_Surface* nine = NULL;
 	dollarsign = IMG_Load ( "dollarsign.png");
 	one = IMG_Load( "one.png" ); 
 	two = IMG_Load( "two.png" ); 
 	three = IMG_Load( "three.png" ); 
 	four = IMG_Load( "four.png" ); 	
 	five = IMG_Load( "five.png" ); 
 	six = IMG_Load( "six.png" ); 
 	seven = IMG_Load( "seven.png" ); 
 	eight = IMG_Load( "eight.png" ); 
 	nine = IMG_Load( "nine.png" ); 
 	zero = IMG_Load( "zero.png" );
 	SDL_Rect numberlocationRect;

 	numberlocationRect.x = x;
	numberlocationRect.y = y;
	SDL_BlitSurface( dollarsign, NULL, screen, &numberlocationRect );

	if(number%10 == 0)
	{
		numberlocationRect.x = x+90;
		numberlocationRect.y = y;
		SDL_BlitSurface( zero, NULL, screen, &numberlocationRect ); 
	}
	if(number%10 == 1)
	{
		numberlocationRect.x = x+90;
		numberlocationRect.y = y;
		SDL_BlitSurface( one, NULL, screen, &numberlocationRect ); 
	}
	if(number%10 == 2)
	{
		numberlocationRect.x = x+90;
		numberlocationRect.y = y;
		SDL_BlitSurface( two, NULL, screen, &numberlocationRect );
	}
	if(number%10 == 3)
	{		
		numberlocationRect.x = x+90;
		numberlocationRect.y = y;
		SDL_BlitSurface( three, NULL, screen, &numberlocationRect ); 
	}
	if(number%10 == 4)
	{
		numberlocationRect.x = x+90;
		numberlocationRect.y = y;
		SDL_BlitSurface( four, NULL, screen, &numberlocationRect ); 
	}
	if(number%10 == 5)
	{
		numberlocationRect.x = x+90;
		numberlocationRect.y = y;
		SDL_BlitSurface( five, NULL, screen, &numberlocationRect ); 
	}
	if(number%10 == 6)
	{
		numberlocationRect.x = x+90;
		numberlocationRect.y = y;
		SDL_BlitSurface( six, NULL, screen, &numberlocationRect ); 
	}
	if(number%10 == 7)
	{	
		numberlocationRect.x = x+90;
		numberlocationRect.y = y;
		SDL_BlitSurface( seven, NULL, screen, &numberlocationRect ); 
	}
	if(number%10 == 8)
	{
		numberlocationRect.x = x+90;
		numberlocationRect.y = y;
		SDL_BlitSurface( eight, NULL, screen, &numberlocationRect ); 
	}
	if(number%10 == 9)
	{	
		numberlocationRect.x = x+90;
		numberlocationRect.y = y;
		SDL_BlitSurface( nine, NULL, screen, &numberlocationRect ); 
	}


	if((number/10)%10 == 0)
	{

		numberlocationRect.x = x+67;
		numberlocationRect.y = y;
		SDL_BlitSurface( zero, NULL, screen, &numberlocationRect ); 
		}
	if((number/10)%10 == 1)
	{
		numberlocationRect.x = x+67;
		numberlocationRect.y = y;
		SDL_BlitSurface( one, NULL, screen, &numberlocationRect ); 
	}
	if((number/10)%10 == 2)
	{
		numberlocationRect.x = x+67;
		numberlocationRect.y = y;
		SDL_BlitSurface( two, NULL, screen, &numberlocationRect ); 
	}
	if((number/10)%10 == 3)
	{
		numberlocationRect.x = x+67;
		numberlocationRect.y = y;
		SDL_BlitSurface( three, NULL, screen, &numberlocationRect ); 
	}
	if((number/10)%10 == 4)
	{
		numberlocationRect.x = x+67;
		numberlocationRect.y = y;
		SDL_BlitSurface( four, NULL, screen, &numberlocationRect ); 
	}
	if((number/10)%10 == 5)
	{
		numberlocationRect.x = x+67;
		numberlocationRect.y = y;
		SDL_BlitSurface( five, NULL, screen, &numberlocationRect );
	}
	if((number/10)%10 == 6)
	{
		numberlocationRect.x = x+67;
		numberlocationRect.y = y;
		SDL_BlitSurface( six, NULL, screen, &numberlocationRect ); 
	}
	if((number/10)%10 == 7)
	{
		numberlocationRect.x = x+67;
		numberlocationRect.y = y;
		SDL_BlitSurface( seven, NULL, screen, &numberlocationRect ); 
	}
	if((number/10)%10 == 8)
	{
		numberlocationRect.x = x+67;
		numberlocationRect.y = y;
		SDL_BlitSurface( eight, NULL, screen, &numberlocationRect ); 
	}
	if((number/10)%10 == 9)
	{
		numberlocationRect.x = x+67;
		numberlocationRect.y = y;
		SDL_BlitSurface( nine, NULL, screen, &numberlocationRect ); 
	}
	if((number/100)%10 == 0)
	{

		numberlocationRect.x = x+45;
		numberlocationRect.y = y;
		SDL_BlitSurface( zero, NULL, screen, &numberlocationRect ); 
	}
	if((number/100)%10 == 1)
	{
		numberlocationRect.x = x+45;
		numberlocationRect.y = y;
		SDL_BlitSurface( one, NULL, screen, &numberlocationRect ); 
	}
	if((number/100)%10 == 2)
	{
		numberlocationRect.x = x+45;
		numberlocationRect.y = y;
		SDL_BlitSurface( two, NULL, screen, &numberlocationRect ); 
	}
	if((number/100)%10 == 3)
	{
		numberlocationRect.x = x+45;
		numberlocationRect.y = y;
		SDL_BlitSurface( three, NULL, screen, &numberlocationRect ); 
	}
	if((number/100)%10 == 4)
	{
		numberlocationRect.x = x+45;
		numberlocationRect.y = y;
		SDL_BlitSurface( four, NULL, screen, &numberlocationRect ); 
	}
	if((number/100)%10 == 5)
	{
		numberlocationRect.x = x+45;
		numberlocationRect.y = y;
		SDL_BlitSurface( five, NULL, screen, &numberlocationRect ); 
	}
	if((number/100)%10 == 6)
	{
		numberlocationRect.x = x+45;
		numberlocationRect.y = y;
		SDL_BlitSurface( six, NULL, screen, &numberlocationRect ); 
	}
	if((number/100)%10 == 7)
	{
		numberlocationRect.x = x+45;
		numberlocationRect.y = y;
		SDL_BlitSurface( seven, NULL, screen, &numberlocationRect ); 
	}
	if((number/100)%10 == 8)
	{
		numberlocationRect.x = x+45;
		numberlocationRect.y = y;
		SDL_BlitSurface( eight, NULL, screen, &numberlocationRect ); 
	}
	if((number/100)%10 == 9)
	{
		numberlocationRect.x = x+45;
		numberlocationRect.y = y;
		SDL_BlitSurface( nine, NULL, screen, &numberlocationRect ); 
	}
	if((number/1000)%10 == 0)
	{
		numberlocationRect.x = x+22;
		numberlocationRect.y = y;
		SDL_BlitSurface( zero, NULL, screen, &numberlocationRect ); 
	}
	if((number/1000)%10 == 1)
	{
		numberlocationRect.x = x+22;
		numberlocationRect.y = y;
		SDL_BlitSurface( one, NULL, screen, &numberlocationRect ); 
	}
	if((number/1000)%10 == 2)
	{
		numberlocationRect.x = x+22;
		numberlocationRect.y = y;
		SDL_BlitSurface( two, NULL, screen, &numberlocationRect ); 
	}
	if((number/1000)%10 == 3)
	{
		numberlocationRect.x = x+22;
		numberlocationRect.y = y;
		SDL_BlitSurface( three, NULL, screen, &numberlocationRect ); 
	}
	if((number/1000)%10 == 4)
	{
		numberlocationRect.x = x+22;
		numberlocationRect.y = y;
		SDL_BlitSurface( four, NULL, screen, &numberlocationRect ); 
	}
	if((number/1000)%10 == 5)
	{
		numberlocationRect.x = x+22;
		numberlocationRect.y = y;
		SDL_BlitSurface( five, NULL, screen, &numberlocationRect ); 
	}
	if((number/1000)%10 == 6)
	{
		numberlocationRect.x = x+22;
		numberlocationRect.y = y;
		SDL_BlitSurface( six, NULL, screen, &numberlocationRect ); 
	}
	if((number/1000)%10 == 7)
	{
		numberlocationRect.x = x+22;
		numberlocationRect.y = y;
		SDL_BlitSurface( seven, NULL, screen, &numberlocationRect ); 
	}
	if((number/1000)%10 == 8)
	{
		numberlocationRect.x = x+22;
		numberlocationRect.y = y;
		SDL_BlitSurface( eight, NULL, screen, &numberlocationRect );
	}
	if((number/1000)%10 == 9)
	{
		numberlocationRect.x = x+22;
		numberlocationRect.y = y;
		SDL_BlitSurface( nine, NULL, screen, &numberlocationRect ); 
	}

 	SDL_FreeSurface( dollarsign);
	SDL_FreeSurface( zero);
 	SDL_FreeSurface( one);
	SDL_FreeSurface( two); 	
	SDL_FreeSurface( three);
	SDL_FreeSurface( four); 	
	SDL_FreeSurface( five);
	SDL_FreeSurface( six); 	
	SDL_FreeSurface( seven);
	SDL_FreeSurface( eight); 	
	SDL_FreeSurface( nine);
	return screen;
}







