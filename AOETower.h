/* Katie Loughran
	Thom Behrens
	Matthew Staffelbach
 	
	Tower Defense Project
	
	Area of Effect Tower Class Header File
*/



#ifndef AOETOWER_H
#define AOETOWER_H

# include <iostream>
# include "SDL/SDL.h"
# include <SDL/SDL_image.h>
#include <vector>
# include "griddy.h"

using namespace std;

class AOETower: public Tower{
	public:
		AOETower(int = 0, int = 0, int = 0); // inputs are "level" of tower (upgrade), x coord and y coord
		
		virtual void Upgrade();
		int getBlastRadius();
		virtual void Attack(vector <Enemy>&, SDL_Surface *, int); // searches for enemy and calls sendShot function
		void sendShot( SDL_Rect, SDL_Surface *, int); // prints a shot being fired on the screen
		int sign(int, int); // find direction in which shot should fire

	
	private:
		int blast_radius;
		int blast_multiplier; // adjusts proportional blast_radius value;
		SDL_Surface * bomb;
		SDL_Surface * explosion;//only one is printed at a time (so only one location), but we need two different images (one exploding, one non exploding)
		SDL_Rect shotRect;
};

#endif
