/* Katie Loughran
	Thom Behrens
	Matthew Staffelbach
 	
	Tower Defense Project
	
	Pulse (PULS) Tower Class Implemntation
*/

# include <iostream>
# include "SDL/SDL.h"
# include <SDL/SDL_image.h>
#include <vector>
# include "griddy.h"
# include "Tower.h"
# include "PULSTower.h"
# include <cmath>

using namespace std;

//\\// PULSE Tower Class Constructor

PULSTower::PULSTower(  int lev, int x, int y  ): Tower( lev ){
	setType(2);
	drawTower(getType());
	settowerRectx(x);
	settowerRecty(y);
	Upgrade();
	smallPulse = IMG_Load("raybeam3.png"); // loads images for raybeams
	medPulse = IMG_Load("raybeam2.png");
	bigPulse = IMG_Load("raybeam.png");
}
//\\//
void PULSTower::Upgrade(){
		setRange(3);
		setDamage(2);
		setTowerCost(2); // basically extension of constructor
}
//\\//
void PULSTower::Attack(vector <Enemy>& enemy_vec, SDL_Surface * screen, int treescroll){
	int fire = 0;
	int itr  = 0;
	for(itr = 0; itr <= enemy_vec.size(); itr++) {
		if(getRange() >= sqrt(pow(abs(enemy_vec[itr].getguyRect().x-(gettowerRect().x + gettowerRect().w/2)),2)+pow(abs(enemy_vec[itr].getguyRect().y-(gettowerRect().y + gettowerRect().h/2)),2)) // finds hypotenuse between tower and enemy and sees if it is within range
		&& enemy_vec[itr].getType() != 5) {
			fire = 1;
			break;
		}
	}
	if (fire) {
		sendShot(enemy_vec[itr].getguyRect(), screen, treescroll);
		for(itr = 0; itr <= enemy_vec.size(); itr++) { // unlike shot tower, this iterates through the enemy_vec and finds ALL enemies in the range.
			if(getRange() >= sqrt(pow(abs(enemy_vec[itr].getguyRect().x-(gettowerRect().x + gettowerRect().w/2)),2)+pow(abs(enemy_vec[itr].getguyRect().y-(gettowerRect().y + gettowerRect().h/2)),2)) && enemy_vec[itr].getType() != 5) {
				enemy_vec[itr].setArmour(enemy_vec[itr].getArmour() - getDamage());
			}
		}
	}
}
//\\//
void PULSTower::sendShot( SDL_Rect guyRect, SDL_Surface * screen, int treescroll) {
	PulseRect.x = gettowerRect().x-50;
	PulseRect.y = gettowerRect().y-50;
	switch (treescroll%3) {
		case 0:
			SDL_BlitSurface( smallPulse, NULL, screen, &PulseRect );
			SDL_Flip( screen );
			SDL_Delay(2); // uses treescroll over time to create radiating effect
			break;
		case 1:
			SDL_BlitSurface( medPulse, NULL, screen, &PulseRect );
			SDL_Flip( screen );
			SDL_Delay(2);
			break;
		case 2:
			SDL_BlitSurface( bigPulse, NULL, screen, &PulseRect );
			SDL_Flip( screen );
			SDL_Delay(2);
			break;
		default:
			break;
	}
}
