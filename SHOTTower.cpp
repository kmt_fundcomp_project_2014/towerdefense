/* Katie Loughran
	Thom Behrens
	Matthew Staffelbach
 	
	Tower Defense Project
	
	Shooter (SHOT) Tower Class Implemntation
*/

#include "SDL/SDL.h"
#include <SDL/SDL_image.h>
#include <string>
#include <iostream>
#include <vector>
#include "griddy.h"
#include <sstream>
#include <stdio.h>      /* printf, NULL */
#include <stdlib.h>     /* srand, rand */
#include <time.h>       /* time */
#include "src.h"
#include "Tower.h"
#include "AOETower.h"
#include "PULSTower.h"
#include "SHOTTower.h"
#include "Enemy.h"
#include "tdfunctions.h"
#include "pathVecFunc.h"
# include <cmath>

using namespace std;

//\\// Shooter Tower Class Constructor

SHOTTower::SHOTTower(  int lev, int x, int y  ): Tower( lev ){ // basic constructor
	setType(3);
	drawTower( getType() );
	settowerRectx(x);
	settowerRecty(y);
	Upgrade();
	shot = IMG_Load("shot.png");
}

void SHOTTower::Upgrade(){				//Basically extension of constructor
		setRange(3);
		setDamage(3);
		setTowerCost(1);
}
//\\//
void SHOTTower::Attack(vector <Enemy>& enemy_vec, SDL_Surface * screen, int treescroll){
	int fire = 0;
	int itr  = 0;
	for(itr = 0; itr <= enemy_vec.size(); itr++) {
		if(getRange() >= sqrt(pow(abs(enemy_vec[itr].getguyRect().x-(gettowerRect().x + gettowerRect().w/2)),2)+pow(abs(enemy_vec[itr].getguyRect().y-(gettowerRect().y + gettowerRect().h/2)),2)) // Sqrt(pow(abs()) + pow(abs())) checks if distance between enemy and tower is within range
		&& enemy_vec[itr].getType() != 5) {
			fire = 1; // if it is within range, fire.
			break;
		}
	}
	if (fire) {
		sendShot(enemy_vec[itr].getguyRect(), screen, treescroll);  // send animation
		enemy_vec[itr].setArmour(enemy_vec[itr].getArmour() - (getDamage()*3)); // take away health from applicable enemy.
	}
}
//\\//
void SHOTTower::sendShot( SDL_Rect guyRect, SDL_Surface * screen, int treescroll){
	int xdist = abs(guyRect.x - (gettowerRect().x + gettowerRect().w/2)); // deltaX between tower and enemy
	int ydist = abs(guyRect.y - (gettowerRect().y + gettowerRect().h/2));	// deltaY between tower and enemy
	int xsign = sign((gettowerRect().x + gettowerRect().w/2), guyRect.x); // direction on x-axis bullet should travel
	int ysign = sign((gettowerRect().y + gettowerRect().h/2), guyRect.y); // direction on y-axis bullet should travel
	int shotdist = 0;
	int enemydist = sqrt(pow(abs(guyRect.x-(gettowerRect().x + gettowerRect().w/2)),2)+pow(abs(guyRect.y-(gettowerRect().y + gettowerRect().h/2)),2)); // distance from enemy to tower
	for(int x = (gettowerRect().x + gettowerRect().w/2), y = (gettowerRect().y + gettowerRect().h/2); 
	enemydist >= shotdist;
	x = x+(xdist / 20 * xsign), y = y+(ydist / 20 * ysign)) {
		shotRect.x = x;
		shotRect.y = y;
		SDL_BlitSurface( shot, NULL, screen, &shotRect );								 // prints bullets along hypotenuse
		SDL_Flip( screen );
		SDL_Delay(2);
		shotdist = sqrt(pow(abs(x-(gettowerRect().x + gettowerRect().w/2)),2) + pow(abs(y-(gettowerRect().y + gettowerRect().h/2)),2));
	}
}
//\\//
int SHOTTower::sign(int from, int to) {
	if(from > to) return -1;
	else return 1;
}
