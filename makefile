all:	main
TOWER= Tower.o Enemy.o AOETower.o SHOTTower.o PULSTower.o
FUNC= src.o griddy.o tdfunctions.o pathVecFunc.o 

main:	main.o $(FUNC) $(TOWER)
	g++ main.o $(FUNC) $(TOWER) -o main -lSDL -lSDL_image -lSDL_mixer

main.o: main.cpp 
	g++ -c main.cpp -lSDL -lSDL_image -lSDL_mixer

src.o: src.cpp
	g++ -c src.cpp

griddy.o: griddy.cpp
	g++ -c griddy.cpp

tdfunctions.o: tdfunctions.cpp
	g++ -c tdfunctions.cpp

Tower.o: Tower.cpp
	g++ -c Tower.cpp

pathVecFunc.o: pathVecFunc.cpp
	g++ -c pathVecFunc.cpp

Enemy.o: Enemy.cpp
	g++ -c Enemy.cpp

AOETower.o: AOETower.cpp
	g++ -c AOETower.cpp

SHOTTower.o: SHOTTower.cpp
	g++ -c SHOTTower.cpp

PULSTower.o: PULSTower.cpp
	g++ -c PULSTower.cpp

clean:
	rm -f *.o main

