/* Katie Loughran
	Thom Behrens
	Matthew Staffelbach
 	
	Tower Defense Project
	
	Tower Class Implemntation
*/

# include <iostream>
# include "SDL/SDL.h"
# include <SDL/SDL_image.h>
#include <vector>
# include "griddy.h"
# include "Tower.h"

using namespace std;
//\\// Tower Class Constructor
Tower::Tower( int lev ){
	type=1;
	range=1;			
	damage=1;			
	cost=1;			
}
//\\//
//Get 
int Tower::getType(){
	return type;
}
//\\//
int Tower::getTowerCost(){
	return cost;
}
//\\//
int Tower::getRange(){
	return range;
}
//\\//
int Tower::getDamage(){
	return damage;
}
//\\//
//Set
void Tower::setType( int ty ){
	type=ty;
}
//\\//
void Tower::setTowerCost( int cos ){
	cost=36*cos;
}
//\\//
void Tower::setRange( int ran){
	range=20+25*ran;
}
//\\//
void Tower::setDamage( int dam ){
	damage=dam;
}
//\\//
void Tower::settowerRectx(int x){
	towerRect.x = x;
}
//\\//
void Tower::settowerRecty(int y){
	towerRect.y = y;
}
//\\//
SDL_Surface* Tower::gettower(){
	return tower;
} 
//\\//
SDL_Rect Tower::gettowerRect(){
	return towerRect;
}
//\\//
void Tower::drawTower( int typ ){	// draw tower based on type from constructor
	if(typ == 1) { tower = IMG_Load( "fire_tower.bmp" ); 	cout<<"Image 1 loaded"<<endl; }
	else if(typ == 2) { tower = IMG_Load( "wizardtower.png" ); 	cout<<"Image 2 loaded"<<endl; }
	else { tower = IMG_Load( "water_tower.bmp" ); 	cout<<"Image 3 loaded"<<endl; }
}
//\\//
void Tower::freeTower(){	// this command needs to be functionalized since tower is a data member
	 SDL_FreeSurface(tower);
}
//\\//
SDL_Surface* Tower::animate( SDL_Surface *& screen) { // blits tower onto screen
	SDL_BlitSurface( tower, NULL, screen, &towerRect );
	return screen;
}
