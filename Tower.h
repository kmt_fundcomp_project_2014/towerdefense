/* Katie Loughran
	Thom Behrens
	Matthew Staffelbach
 	
	Tower Defense Project
	
	Tower Class Header File
*/



#ifndef TOWER_H
#define TOWER_H

# include <iostream>
# include "SDL/SDL.h"
# include <SDL/SDL_image.h>
#include <vector>
# include "griddy.h"
# include "Enemy.h"

using namespace std;

class Tower{
	public:
		Tower( int = 0); //Default/Non-default Consructor
		virtual void Upgrade() = 0; //PURE Virtual upgrade function that MUST be overriden by derived files, is NOT implemented in Tower.cpp
		virtual void Attack(vector <Enemy>&, SDL_Surface *, int) = 0; // another pure virtual function - this one varies by class more than Upgrade()
		int getTowerCost();				//Basic set/gets
		int getReloadRate();
		int getRange();
		int getDamage();
		int getType();
		void setTowerCost( int );
		void setRange( int );
		void setDamage( int );
		void setType( int );
		void setTarget( Enemy );
		SDL_Surface* gettower();
		SDL_Rect gettowerRect();
		void settowerRectx(int);
		void settowerRecty(int);
		void drawTower(int);
		void freeTower();
		SDL_Surface* animate( SDL_Surface *&); // blits image to screen based on location from towerRect




	private:
		int type;
		int range;				//Range of attack
		int damage;				//Damage of each hi
		int cost;				//Cost to buy the tower
		SDL_Surface* tower;	// image for tower
		SDL_Rect towerRect;	// position for tower
};

#endif
