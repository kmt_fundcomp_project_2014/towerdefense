/* Katie Loughran
	Thom Behrens
	Matthew Staffelbach
 	
	Tower Defense Project
	
	Enemy Class Implemntation
*/

# include <iostream>
# include "Enemy.h"
# include "SDL/SDL.h"
# include <SDL/SDL_image.h>
# include "griddy.h"
#include <vector>


using namespace std;

//\\// Enemy Class Constructor
Enemy::Enemy( int Type ){
	type=Type;																		//Sets the type of enemy 0-5
	drawEnemy( type );														//Sets the image of the enemy, based on the type
	guyRect.x = 0;																//Initial x,y coordinates for guy (the enemy), which will be changed when animated
	guyRect.y = 0;
	update();																			//Update initializes the rest of the data members based on the type of the enemy
}		
//\\//
void Enemy::update(){
if(type == 4 ){																	//DRAGON
		damage=5;
		armour=30;
		armour=300;
		reward=7;
	}
	else if(type == 3 ){													//UNICORN
		damage=5;
		armour=240;
		reward=6;
	}
	else if(type == 2 ){													// GIANT
		damage=3;
		armour=150;
		reward=5;
	}
	else if(type == 1 ){													// BLACK KNIGHT
		damage=3;
		armour=100;
		reward=5;
	}
	else if (type == 0) {													// WHITE KNIGHT
		damage=3;
		armour=100;
		reward=5;
	}
	else{																					// An enemy without a type, this is used as a placeholder in the enemy vector
		type=5;
		armour=0;
	}

}
//\\//
void Enemy::drawEnemy( int picnum )				//Draws the enemy based on which direction the enemy needs to be facing and the type
{																							//For each type, there are four ways in which the enemy can be running (picnum)
	if(type==0){	
	//	SDL_FreeSurface(guy); SDL_FreeSurface(guy2);
		if( picnum == 1 ) { guy = IMG_Load( "guy_on_horse.png" ); guy2 = IMG_Load( "guy_on_horse2.png" ); }
		else if( picnum == 2) { guy = IMG_Load( "guy_on_horse3.png" ); guy2 = IMG_Load( "guy_on_horse6.png" ); }
		else if( picnum == 3 ) { guy = IMG_Load( "guy_on_horse5.png" ); guy2 = IMG_Load( "guy_on_horse8.png" ); }
		else if( picnum == 4 ) { guy = IMG_Load( "guy_on_horse9.png" ); guy2 = IMG_Load( "guy_on_horse10.png" ); }
		//else{ }		
	}
	else if(type==1){
	//	SDL_FreeSurface(guy); SDL_FreeSurface(guy2);
		if( picnum == 1 ) { guy = IMG_Load( "blackknight.png" ); guy2 = IMG_Load( "blackknight2.png" ); }
		else if( picnum == 2) { guy = IMG_Load( "blackknight3.png" ); guy2 = IMG_Load( "blackknight6.png" ); }
		else if( picnum == 3 ) { guy = IMG_Load( "blackknight5.png" ); guy2 = IMG_Load( "blackknight8.png" ); }
		else if( picnum == 4 ) { guy = IMG_Load( "blackknight9.png" ); guy2 = IMG_Load( "blackknight10.png" ); }
		else{ }	
	}
	else if(type==2){
		//SDL_FreeSurface(guy); SDL_FreeSurface(guy2);
		if( picnum == 1 ) { guy = IMG_Load( "giant1.png" ); guy2 = IMG_Load( "giant2.png" ); }
		else if( picnum == 2) { guy = IMG_Load( "giant3.png" ); guy2 = IMG_Load( "giant6.png" ); }
		else if( picnum == 3 ) { guy = IMG_Load( "giant5.png" ); guy2 = IMG_Load( "giant8.png" ); }
		else if( picnum == 4 ) { guy = IMG_Load( "giant9.png" ); guy2 = IMG_Load( "giant10.png" ); }
		else{ }
	}	
	else if(type==3){
	//	SDL_FreeSurface(guy); SDL_FreeSurface(guy2);
		if( picnum == 1 ) { guy = IMG_Load( "unicorn.png" ); guy2 = IMG_Load( "unicorn2.png" ); }
		else if( picnum == 2) { guy = IMG_Load( "unicorn3.png" ); guy2 = IMG_Load( "unicorn6.png" ); }
		else if( picnum == 3 ) { guy = IMG_Load( "unicorn5.png" ); guy2 = IMG_Load( "unicorn8.png" ); }
		else if( picnum == 4 ) { guy = IMG_Load( "unicorn9.png" ); guy2 = IMG_Load( "unicorn10.png" ); }
		else{ }
	}
	else if(type==4){
		//SDL_FreeSurface(guy); SDL_FreeSurface(guy2);
		if( picnum == 1 ) { guy = IMG_Load( "dragon.png" ); guy2 = IMG_Load( "dragon2.png" ); }
		else if( picnum == 2) { guy = IMG_Load( "dragon3.png" ); guy2 = IMG_Load( "dragon6.png" ); }
		else if( picnum == 3 ) { guy = IMG_Load( "dragon5.png" ); guy2 = IMG_Load( "dragon8.png" ); }
		else if( picnum == 4 ) { guy = IMG_Load( "dragon9.png" ); guy2 = IMG_Load( "dragon10.png" ); }
		else{ }
	}

	else{																	
	//	SDL_FreeSurface(guy); SDL_FreeSurface(guy2);
		guy = IMG_Load( "ground2.png" ); guy2 = IMG_Load( "ground2.png" );							//ground2.png is a 25x25 pixel transparent png, so it is see-through
	}
}
//\\//

void Enemy::freeEnemy()											//Frees SDL_Surface objects
{
	 SDL_FreeSurface(guy);
	 SDL_FreeSurface(guy2);
	cout << "enemy freed" << endl;
}
//\\//																					//Get functions
int Enemy::getDamage(){
	return damage;
}
//\\//
int Enemy::getArmour(){ 
	return armour;
}
//\\//
int Enemy::getReward(){ 
	return reward;
}
//\\//
int Enemy::getType(){ 
	return type;
}	
//\\//
SDL_Surface* Enemy::getguy(){
	return guy;
} 
//\\//
SDL_Surface* Enemy::getguy2(){
	return guy2;
}
SDL_Rect Enemy::getguyRect(){
	return guyRect;
}
//\\//																			//Set functions
void Enemy::setType( int ty ){ 
	type=ty;
	update();
}
//\\//
void Enemy::setDamage( int dam ){ 
	damage=dam;
}
//\\//
void Enemy::setArmour( int arm ){ 
	armour=arm;
}
//\\//
void Enemy::setReward( int rew ){ 
	reward=rew;
}
//\\//
void Enemy::setguyRectx(int x){
	guyRect.x = x;
}
//\\//
void Enemy::setguyRecty(int y){
	guyRect.y = y;
}
//\\///																		//Animate function
SDL_Surface* Enemy::animate( SDL_Surface *& screen, vector<griddy>::iterator i, int treescroll, int delay) {
		//This section chooses which direction the enemy should face based on where it will be next (looks at path_vec)
		if((i-delay)->getxcoord() < 30){drawEnemy(1);}	// This sets the initial diration as forward
		if((i-delay)->getimage()==7 && ( (i-delay+1)-> getxcoord() == (i-delay)-> getxcoord() ) ){drawEnemy(3);}
		if((i-delay)->getimage()==7 && ( (i-delay+1)-> getxcoord() != (i-delay)-> getxcoord() ) ){drawEnemy(4);}
		if((i-delay)->getimage()==8 && ( (i-delay+1)-> getxcoord() == (i-delay)-> getxcoord() ) ){drawEnemy(2);}
		if((i-delay)->getimage()==8 && ( (i-delay+1)-> getxcoord() != (i-delay)-> getxcoord() ) ){drawEnemy(1);}
		if((i-delay)->getimage()==9 && ( (i-delay+1)-> getxcoord() == (i-delay)-> getxcoord() ) ){drawEnemy(3);}
		if((i-delay)->getimage()==9 && ( (i-delay+1)-> getxcoord() != (i-delay)-> getxcoord() ) ){drawEnemy(1);}
		if((i-delay)->getimage()==6 && ( (i-delay+1)-> getxcoord() == (i-delay)-> getxcoord() ) ){drawEnemy(2);}
		if((i-delay)->getimage()==6 && ( (i-delay+1)-> getxcoord() != (i-delay)-> getxcoord() ) ){drawEnemy(4);}
		
		//Sets the x and y coordinates based on where the enemy is on the path
		guyRect.x = ((i-delay)->getxcoord());	
		guyRect.y = ((i-delay)->getycoord());

		//Every other refreshment of the screen, the guy changes his images and position so it looks like he is running
		if( treescroll % 2 == 1 ){
			SDL_BlitSurface( guy, NULL, screen, &guyRect );
		}
		else{	
			//These if statements change the enemy's x coordinate so he only goes half a block (vs a whole block) 
				//forward every time he moves. This helps smooth out the animation
			if(guyRect.x < ((i-delay)+1)->getxcoord()){
				guyRect.x = ((i-delay)->getxcoord()+12.5);
			}
			if(guyRect.x > ((i-delay)+1)->getxcoord()){
				guyRect.x = ((i-delay)->getxcoord()-12.5);
			}
			if(guyRect.y < ((i-delay)+1)->getycoord()){
				guyRect.y = ((i-delay)->getycoord()+12.5);
			}
			if(guyRect.y > ((i-delay)+1)->getycoord()){
				guyRect.y = ((i-delay)->getycoord()-12.5);
			}
			SDL_BlitSurface( guy2, NULL, screen, &guyRect );
		}

		//The enemy has been drawn, return the screen
	return screen;
}
