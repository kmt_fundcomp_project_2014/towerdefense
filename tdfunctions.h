#ifndef TDFUNCTIONS_H
#define TDFUNCTIONS_H

#include <vector>
#include "Enemy.h"
int delete_enemies( vector< Enemy > &enemy_vec );

void create_enemies( int wave, vector< Enemy > &enemy_vec);

SDL_Surface* makemountains(vector<vector<griddy> > &vec2D, SDL_Surface* screen);

void makemidground(vector<vector<griddy> > &vec2D);

void makeNULLS(vector<vector<griddy> > &vec2D);

SDL_Surface* changetrees(vector<vector<griddy> > &vec2D, int treescroll, SDL_Surface* screen);

void setXsandYs(vector<vector<griddy> > &vec2D);

SDL_Surface* printtowercost(int number, int x, int y, vector<vector<griddy> > &vec2D, SDL_Surface* screen);

void makegrounddefault(vector<vector<griddy> > &vec2D);

int checkforaddition(vector<vector<griddy> > &vec2D, int x, int y);

SDL_Surface* countingFunc(int number, int x, int y, vector<vector<griddy> > &vec2D, SDL_Surface*);

SDL_Surface* printEnemyCount(int number, int x, int y, vector<vector<griddy> > &vec2D, SDL_Surface* screen, int which);

SDL_Surface* toweroptions(vector<vector<griddy> > &vec2D, SDL_Surface* screen);

int placetower(int x, int y);

#endif
