#include <vector>
#include "griddy.h"
#include "SDL/SDL.h"
#include <SDL/SDL_image.h>
#include "pathVecFunc.h"
#include <sstream>
#include <fstream>
#include <string>


void makepathtwo(int map, vector<vector<griddy> > &vec2D, vector<griddy> &path_vec){

	string filename;																																//Creates a filename depending on the int input map to decide which path to draw
	if (map == 2) filename="map2.txt";
	else if (map == 1) filename="map1.txt";
	else filename="map3.txt";

	griddy new_path;																															//Creates a new object of type griddy that will act as a temp object to get pushed
																																									//into the path_vec vector

	ifstream boardFile;																														//Makes a ifstream object in order to open file
	boardFile.open (filename.c_str(), ios::in);																				//Open file to make it readable
	if( !boardFile){																																//Checks to make sure file was opened
		cout<<"File could not be opened! "<<endl;
	}else{																																				//Else - file was opened successfully
		while( !boardFile.eof() ){																										//Until the end of the file (EOF) is reached
			string line;																																//Will contain the whole line as a string
			int value;																																	//Sets value of type int
			while( getline(boardFile, line) ){																						//While there are still lines in boardfile to assigned to line variable
				vector<int> row;
				stringstream splitting(line);																							//Puts the line into splitting
				for( int i=0; i < 3; i++){
					splitting >> value; 																										//This outputs each item of splitting delimiting spaces into value
					row.push_back(value);																								//Puts all the values in the line into the row by placing them at the end
				}
				new_path.setycoord(row[2]*25);																					//Sets the new_path objects x,y coordinates and image number
				new_path.setxcoord(row[1]*25);
				new_path.setimage(row[0]);
				path_vec.push_back(new_path);																					//Add new_path object to the path_vec vector
				int ps = row[1];
				int mw = row[2];
				vec2D[ps][mw].setimage(row[0]);																				//Changes the 2D vector that keeps track of the board to include the path
				vec2D[ps][mw].setxsize(1); 																						//Not necessary because this is the default
				vec2D[ps][mw].setysize(1);
											
			}	
		}
		boardFile.close();																														//Close file
	}
}


void placetrees(vector<vector<griddy> > &vec2D, vector<griddy> &path_vec){
 for(int xiterator = 0; xiterator < 35; xiterator++){															//For the width of the board
	 for(int yiterator = 5; yiterator < 22; yiterator++){														//For the height of the board
		if(vec2D[xiterator][yiterator].getimage() == 4 && (rand() % 15) == 5){			//If the place is empty( just grass ) and a random number 1-15 is a 5
			vec2D[xiterator][yiterator].setimage((rand() % 3) +11);										//Set the image to a random tree position image
			vec2D[xiterator][yiterator].setstat(0);																			//Set stat of the position in the 2D vector to occupied
		}
	 }
  }
}

