
#include"griddy.h"

griddy::griddy()  {
	xcoord = 0; 
	ycoord = 0;  
	image = NULL; //image 1 is the mountains, 2 is the midground 3 is a tree...
	ysize = 1; // 1 = 25, 2 = 50
	xsize = 1;
	stat = 0; // this differs depending on what the object is
}

void griddy::setxcoord(int x){
	xcoord = x;
}

void griddy::setycoord(int y){
	ycoord = y;
}

void griddy::setxsize(int xsizey){
	xsize = xsizey;
}

void griddy::setysize(int ysizey){
	ysize = ysizey;
}

void griddy::setstat(int staty){
	stat = staty;
}

void griddy::setimage(int imagey){
	image = imagey;
}

int griddy::getxcoord(){
	return xcoord;
}

int griddy::getimage(){
	return image;
}

int griddy::getstat(){
	return stat;
}

int griddy::getycoord(){
	return ycoord;
}

int griddy::getysize(){
	return ysize;
}

int griddy::getxsize(){
	return xsize;
}
