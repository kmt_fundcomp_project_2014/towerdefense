// developing methods of promting display from
// within the constructors of object classes.

#include "SDL/SDL.h"
#include <SDL/SDL_image.h>
#include <string>
#include <iostream>
#include <vector>
#include "griddy.h"
#include <sstream>
#include <stdio.h>      /* printf, NULL */
#include <stdlib.h>     /* srand, rand */
#include <time.h>       /* time */
#include "src.h"
# include "Tower.h"
# include "AOETower.h"
# include "PULSTower.h"
# include "SHOTTower.h"
# include "Enemy.h"

//#include "SDL_gfxPrimitives.h"
using namespace std;
int main(int argc, char* args[] ){
  srand (time(NULL));
  int x = 0;
  int y = 0;
  int running = 1;
  int treescroll = 3;
  

  
  griddy blocks;
  vector<vector<griddy> > vec2D;
vector<griddy> path_vec;
vector<AOETower> tower_vec;
vector<Enemy> enemy_vec;
  string line;
  griddy value;
  int theimage = 3;
  Enemy enemy;
 for(int hk = 0; hk < 35; ++hk){
vector<griddy> newLine;
//stringstream split(line); // split now has the line
for(int jk = 0; jk < 25; ++jk){
// split >> value; // this output the next space delimited thing (first a, then b, then c.. and so on)
newLine.push_back(value);
 }
vec2D.push_back(newLine);
 }


//The images
enemy.drawEnemy();
SDL_Surface* tower = NULL;
//SDL_Surface* guy = NULL;
//SDL_Surface* guy2 = NULL;
SDL_Surface* giant1 = NULL;
SDL_Surface* giant2 = NULL;
SDL_Surface* giant3 = NULL;
SDL_Surface* giant4 = NULL;
SDL_Surface* treeUp = NULL;
SDL_Surface* treeLeft = NULL;
SDL_Surface* treeLeftish = NULL;
SDL_Surface* treeRight = NULL;
SDL_Surface* treeRightish = NULL;
SDL_Surface* mountain = NULL;
SDL_Surface* midground = NULL;
SDL_Surface* ground = NULL;
SDL_Surface* picture = NULL;
SDL_Surface* pathRight = NULL;
SDL_Surface* pathDownRight = NULL;
SDL_Surface* pathLeftDown = NULL;
SDL_Surface* pathLeftUp = NULL;
SDL_Surface* pathUpDown = NULL;
SDL_Surface* pathUpRight = NULL;
SDL_Surface* waterTower = NULL;
SDL_Surface* screen = NULL;
SDL_Rect towerRect; // This is used to locate the placement of the tower later
SDL_Rect treelocationRect;
SDL_Rect mountainlocationRect;
SDL_Rect midgroundlocationRect;
//SDL_Rect guyRect;
//guyRect.x = 0;
//guyRect.y = 0;
mountainlocationRect.x = 0;
mountainlocationRect.y = 0;
towerRect.x = x ;
towerRect.y = y ;
treelocationRect.x = 300;
treelocationRect.y = 300;
midgroundlocationRect.x = 0;
midgroundlocationRect.y = 75;
int sidemove = 0;
int vertmove = 0;
//Start SDL
running=start_SDL();


//Set up screen
screen = SDL_SetVideoMode( 875, 625, 32, SDL_SWSURFACE );
if ( screen == NULL){
running = 0;
}
//Load image
tower = SDL_LoadBMP( "water_tower.bmp" );
//guy = IMG_Load( "guy_on_horse.png" );
//guy2 = IMG_Load( "guy_on_horse2.png" );
giant1 = IMG_Load( "giant1.png" );
giant2 = IMG_Load( "giant2.png" );
giant3 = IMG_Load( "giant3.png" );
giant4 = IMG_Load( "giant4.png" );
treeUp = SDL_LoadBMP( "treeUp.bmp"); //3
treeLeftish = SDL_LoadBMP( "treeLeftish.bmp"); //11
treeLeft = SDL_LoadBMP( "treeLeft.bmp");//12
treeRightish = SDL_LoadBMP( "treeRightish.bmp");//13
treeRight = SDL_LoadBMP( "treeRight.bmp");//14
mountain = SDL_LoadBMP ("mountain.bmp" );//1
midground = SDL_LoadBMP ("midground.bmp" ); //2
ground = SDL_LoadBMP ("ground.bmp");//4
pathRight = SDL_LoadBMP ("pathRight.bmp"); //5
pathLeftDown = SDL_LoadBMP("pathLeftDown.bmp");//6
pathLeftUp = SDL_LoadBMP("pathLeftUp.bmp");//7
pathDownRight = SDL_LoadBMP("pathDownRight.bmp");//8
pathUpRight = SDL_LoadBMP("pathUpRight.bmp");//9
pathUpDown = SDL_LoadBMP("pathUpDown.bmp");//10
waterTower = SDL_LoadBMP("water_tower.bmp"); //15

// Making Ground the default
for( int fe = 0; fe < 35; fe++){ // This is just setting all the squares to ground
for (int xs = 0; xs < 25; xs++){
vec2D[fe][xs].setimage(4);
vec2D[fe][xs].setxsize(1);
vec2D[fe][xs].setysize(1);
vec2D[fe][xs].setstat(0);
}
}

// MAKING MOUNTAINS
for( int we = 0; we < 35; we+=3){ // This is since the mountains are along the x-axis and are of xsize = 3
vec2D[we][0].setimage(1); // the first image occupies [0][0] -  [2][2]
vec2D[we][0].setxsize(3);
vec2D[we][0].setysize(3);
}
// Making Midgrounds

//Midground Animation
for(int re = 0; re < 35; re+=2){
vec2D[re][3].setimage(2);
vec2D[re][3].setxsize(2);
vec2D[re][3].setysize(2);

}

while (!path_vec.empty() ){
path_vec.pop_back();
}

// Making a Path
int mw = 12;
int count=0;
for(int ps = 0; ps < 35; ps++){
vec2D[ps][mw].setimage(5);
vec2D[ps][mw].setxsize(1); // not necessary bc this is the default
vec2D[ps][mw].setysize(1);

griddy new_griddy;
new_griddy.setycoord(mw*25);
new_griddy.setxcoord(ps*25);
path_vec.push_back(new_griddy);
count++;

if((rand() % 7 + 1) == 5 && mw > 6 && ps < 33){ // this makes random up then right things (that have a chance of being long
ps++;
vec2D[ps][mw].setimage(7); //up_bend
new_griddy.setycoord(mw*25);
new_griddy.setxcoord(ps*25);
path_vec.push_back(new_griddy);
count++;
if((rand() % 2 + 1) == 1 && mw > 7){
mw--;
vec2D[ps][mw].setimage(10);
new_griddy.setycoord(mw*25);
new_griddy.setxcoord(ps*25);
path_vec.push_back(new_griddy);
count++;
}
if((rand() % 2 + 1) == 1 && mw > 7){
mw--;
vec2D[ps][mw].setimage(10);
new_griddy.setycoord(mw*25);
new_griddy.setxcoord(ps*25);
path_vec.push_back(new_griddy); count++;

}
if((rand() % 2 + 1) == 1 && mw > 7){
mw--;
vec2D[ps][mw].setimage(10);
new_griddy.setycoord(mw*25);
new_griddy.setxcoord(ps*25);
path_vec.push_back(new_griddy); count++;
}
mw--;
vec2D[ps][mw].setimage(8);
new_griddy.setycoord(mw*25); new_griddy.setxcoord(ps*25);
path_vec.push_back(new_griddy); count++;
}
if((rand() % 7 + 1) == 5 && mw < 22 && ps<33){ // this makes random down then right things (that have a chance of being long)
ps++;
vec2D[ps][mw].setimage(6);
new_griddy.setycoord(mw*25); new_griddy.setxcoord(ps*25);
path_vec.push_back(new_griddy); count++;
if((rand() % 2 + 1) == 1 && mw > 7 && mw<22){
mw++;
vec2D[ps][mw].setimage(10);
new_griddy.setycoord(mw*25); new_griddy.setxcoord(ps*25);
path_vec.push_back(new_griddy); count++;
}
if((rand() % 2 + 1) == 1 && mw > 7 && mw<22){
mw++;
vec2D[ps][mw].setimage(10);
new_griddy.setycoord(mw*25); new_griddy.setxcoord(ps*25);
path_vec.push_back(new_griddy); count++;
}
if((rand() % 2 + 1) == 1 && mw > 7 && mw<22){
mw++;
vec2D[ps][mw].setimage(10);
new_griddy.setycoord(mw*25); new_griddy.setxcoord(ps*25);
path_vec.push_back(new_griddy); count++;
}
mw++;
vec2D[ps][mw].setimage(9);
new_griddy.setycoord(mw*25); new_griddy.setxcoord(ps*25);
path_vec.push_back(new_griddy); count++;
}
}

cout << count <<endl;
// Setting NULLs for covered ground squares
for( int rt = 0; rt < 35; rt++){ // This is just setting all the squares to ground
for (int qw = 0; qw < 25; qw++){
if((vec2D[rt][qw].getxsize() == 2 || vec2D[rt][qw].getxsize() == 3) && rt < 34){vec2D[rt+1][qw].setimage(NULL);}
if((vec2D[rt][qw].getysize() == 2 || vec2D[rt][qw].getysize() == 3) && qw < 24){vec2D[rt][qw+1].setimage(NULL);} // if image has a y size of 2 or 3 then free the image below it
if((vec2D[rt][qw].getysize() == 2 || vec2D[rt][qw].getysize() == 3) && (vec2D[rt][qw].getxsize() == 2 || vec2D[rt][qw].getxsize() == 3) && qw < 24 && rt < 34){vec2D[rt+1][qw+1].setimage(NULL);} // if an image is a 2x2 or 2x3 or 3x3 then free the image in the square down right diag to it
if(vec2D[rt][qw].getxsize() == 3 && rt < 33){vec2D[rt+2][qw].setimage(NULL);}
if(vec2D[rt][qw].getysize() == 3 && qw < 23){vec2D[rt][qw+2].setimage(NULL);}
if(vec2D[rt][qw].getxsize() == 3 && rt < 33 && qw<24 && (vec2D[rt][qw].getysize() == 2 || vec2D[rt][qw].getysize() == 3)){vec2D[rt+2][qw+1].setimage(NULL);}
if(vec2D[rt][qw].getysize() == 3 && rt < 34 && qw<23 && (vec2D[rt][qw].getxsize() == 2 || vec2D[rt][qw].getxsize() == 3)){vec2D[rt+1][qw+2].setimage(NULL);}
if(vec2D[rt][qw].getysize() == 3 && qw < 23){vec2D[rt][qw+2].setimage(NULL);}
if(vec2D[rt][qw].getysize() == 3 && qw < 23 && vec2D[rt][qw].getxsize() == 3 && rt < 33){vec2D[rt+2][qw+2].setimage(NULL);}
}
}




if(tower == NULL || treeLeft == NULL || treeLeftish == NULL || treeRight == NULL || treeUp == NULL || treeRightish == NULL || mountain == NULL || midground == NULL){
    cout<<"failed to load an image"<<endl;
 }

SDL_Event occur;


vector<griddy>::iterator i;
i = path_vec.begin();
vector<AOETower>::iterator j;
j = tower_vec.begin();
AOETower new_tower;

while (running == 1){

   while( SDL_PollEvent( &occur ) ){
       switch( occur.type ){
case SDL_QUIT:
running = 0;
       case SDL_MOUSEBUTTONDOWN:
  switch( occur.button.button ){
case SDL_BUTTON_LEFT:
x = occur.button.x;
y = occur.button.y;
//x = (x - x%25)/25; // this just makes the x and y equal places in the vec2D
//y = (y - y%25)/25;
new_tower.setX(x);
new_tower.setY(y);
tower_vec.push_back(new_tower);
//vec2D[x][y].setimage(theimage);
//vec2D[x][y].setxsize(1);
//vec2D[x][y].setysize(1);


   /* Look for a keypress */
}
case SDL_KEYDOWN:
if(occur.key.keysym.sym == SDLK_q){
running = 0;
}
               /* Check the SDLKey values and move change the coords */
                   switch( occur.key.keysym.sym ){
                      case SDLK_SPACE:
                      running = 0;
                      break;
                      default:
                      break;
               }
           }
}



//picture = SDL_LoadBMP (vec2D[0][0].getimage );



//Setting x and y coords for the Griddy's in Vec2D
for(int ypi = 0; ypi<35; ypi++){
for(int yui = 0; yui <25; yui++){
vec2D[ypi][yui].setxcoord(ypi*25);
vec2D[ypi][yui].setycoord(yui*25);
}
}




//ANIMATE!!

for(int er = 0; er<35; er++){
for(int ter = 0; ter <25; ter++){
treelocationRect.x = vec2D[er][ter].getxcoord();
treelocationRect.y = vec2D[er][ter].getycoord();
if(vec2D[er][ter].getimage() == 1){
SDL_BlitSurface( mountain, NULL, screen, &treelocationRect );
}
if(vec2D[er][ter].getimage() == 2){
SDL_BlitSurface( midground, NULL, screen, &treelocationRect );
}
if(vec2D[er][ter].getimage() == 3){
SDL_BlitSurface( treeUp, NULL, screen, &treelocationRect );
}
if(vec2D[er][ter].getimage() == 4){
SDL_BlitSurface( ground, NULL, screen, &treelocationRect );
}
if(vec2D[er][ter].getimage() == 5){
SDL_BlitSurface( pathRight, NULL, screen, &treelocationRect );
}
if(vec2D[er][ter].getimage() == 6){
SDL_BlitSurface( pathLeftDown, NULL, screen, &treelocationRect );
}
if(vec2D[er][ter].getimage() == 7){
SDL_BlitSurface( pathLeftUp, NULL, screen, &treelocationRect );
}
if(vec2D[er][ter].getimage() == 8){
SDL_BlitSurface( pathDownRight, NULL, screen, &treelocationRect );
}
if(vec2D[er][ter].getimage() == 9){
SDL_BlitSurface( pathUpRight, NULL, screen, &treelocationRect );
}
if(vec2D[er][ter].getimage() == 10){
SDL_BlitSurface( pathUpDown, NULL, screen, &treelocationRect );
}
if(vec2D[er][ter].getimage() == 11){
SDL_BlitSurface( treeLeftish, NULL, screen, &treelocationRect );
}
if(vec2D[er][ter].getimage() == 12){
SDL_BlitSurface( treeLeft, NULL, screen, &treelocationRect );
}
if(vec2D[er][ter].getimage() == 13){
SDL_BlitSurface( treeRightish, NULL, screen, &treelocationRect );
}
if(vec2D[er][ter].getimage() == 14){
SDL_BlitSurface( treeRight, NULL, screen, &treelocationRect );
}
if(vec2D[er][ter].getimage() == 15){
SDL_BlitSurface( waterTower, NULL, screen, &treelocationRect );
vec2D[er][ter].setxsize(2);
vec2D[er][ter].setysize(2);
vec2D[er+1][ter+1].setimage(NULL);
vec2D[er+1][ter].setimage(NULL);
vec2D[er][ter+1].setimage(NULL);

}
}
}


//guyRect.x = i->getxcoord();
//guyRect.y = i->getycoord();


enemy.setguyRectx(i->getxcoord());
enemy.setguyRecty(i->getycoord());

if( treescroll % 2 == 1 ){
SDL_BlitSurface( enemy.getguy(), NULL, screen, &enemy.getguyRect() );
}
else{

if(enemy.getguyRect().x != (i+1)->getxcoord()){
enemy.setguyRectx(i->getxcoord()+12.5);
}
if(enemy.getguyRect().y < (i+1)->getycoord()){
enemy.setguyRecty(i->getycoord()+12.5);
}
if(enemy.getguyRect().y > (i+1)->getycoord()){
enemy.setguyRecty(i->getycoord()-12.5);
}

SDL_BlitSurface( enemy.getguy2(), NULL, screen, &enemy.getguyRect() );
}
////if(sidemove == 1){
////guyRect.x = i->getxcoord();
////}
////if(vertmove != 0){
////guyRect.y = i->getycoord();
////}

if(treescroll%2 ==0){
i++;
}
if( i == path_vec.end() ){
i=path_vec.begin();
}
for( j = tower_vec.begin(); j != tower_vec.end(); j++){
towerRect.x = j->getX();
towerRect.y = j->getY();
SDL_BlitSurface( tower, NULL, screen, &towerRect );
}




//TREE ANIMATION
treescroll++;
if(treescroll % 65 == 0){
for(int u = 5; u < 25; u++){
for(int yu = 0; yu < 35; yu++){


  if(vec2D[yu][u].getimage() == 3 && vec2D[yu][u].getstat() == 0){
vec2D[yu][u].setimage(11);
continue;
  }
  if(vec2D[yu][u].getimage() == 11 && vec2D[yu][u].getstat() == 0){
vec2D[yu][u].setimage(12);
vec2D[yu][u].setstat(1);
continue;
  }
  if(vec2D[yu][u].getimage() == 12){
vec2D[yu][u].setimage(11);
continue;
  }
  if(vec2D[yu][u].getimage() == 11 && vec2D[yu][u].getstat() == 1){
vec2D[yu][u].setimage(3);
continue;
  }
  if(vec2D[yu][u].getimage() == 3 && vec2D[yu][u].getstat() == 1){
vec2D[yu][u].setimage(13);
continue;
  }
  if(vec2D[yu][u].getimage() == 13 && vec2D[yu][u].getstat() == 1){
vec2D[yu][u].setimage(14);
vec2D[yu][u].setstat(0);
continue;
  }
  if(vec2D[yu][u].getimage() == 14){
vec2D[yu][u].setimage(13);
continue;
  }
  if(vec2D[yu][u].getimage() == 13 && vec2D[yu][u].getstat() == 0){
vec2D[yu][u].setimage(3);
continue;
  }
}
}
}


//Update Screen

SDL_Flip( screen );
// Pause
SDL_Delay( 300 );

}


//Quit SDL
//Free the loaded image
SDL_FreeSurface( tower );  SDL_FreeSurface( ground );
SDL_FreeSurface( treeLeftish );SDL_FreeSurface( treeLeft );SDL_FreeSurface( treeUp );SDL_FreeSurface( treeRightish );SDL_FreeSurface( treeRight );
SDL_FreeSurface( mountain );SDL_FreeSurface( giant1 );SDL_FreeSurface( giant2 );SDL_FreeSurface( giant3 );SDL_FreeSurface( giant4 );
SDL_Quit();
  return 0;
}
